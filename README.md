# lookslikematrix Blog

Dies ist der Blog, der unter https://lookslikematrix.de erreichbar ist. Solltet ihr Fehler finden, oder einen Beitrag veröffentlichen wollen, dann fühlt euch frei das Repository zu clonen und einen *Merge Request* zu stellen. Ich freue mich über *Contributer*.

## Getting started
~~~
git clone https://gitlab.com/lookslikematrix/lookslikematrix/
cd lookslikematrix

gem install bundler jekyll
jeykll serve
~~~
