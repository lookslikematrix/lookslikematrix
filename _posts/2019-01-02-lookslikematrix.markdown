---
layout: post
title:  "decker-software-solutions ist jetzt lookslikematrix"
date:   2019-01-02 13:00:00 +0100
categories: stuff
---

2015 habe ich **decker-software-solutions** gegründet und in der Zeit versucht nebenher eine Selbstständigkeit aufzubauen. Nebenher ist das aber leider nicht mehr möglich. Mittlerweile ist man nicht mehr im Studium und ist Papa geworden und daher gibt es jetzt viele andere Sachen nebenher. Ich musste daher das Projekt decker-software-solutions aufgeben, habe mich aber dafür entschieden, dass ich weiter bloggen möchte. Da ich gedanklich mit dem Projekt abschließen wollte, habe ich mich entschieden, dass ich meinen Blog unter einer anderen URL weiter führen werde – [lookslikematrix.de](https://lookslikematrix.de). **Lookslikematrix** war die Idee meiner Frau, da sie oft der Meinung ist, dass die ganzen Sachen aussehen als würde man sich in der Matrix befinden. Vielen Dank für den tollen Namen :). Ich habe die Hoffnung mit lookslikematrix genau dieses Gefühl, dass so manch einer hat, wenn er etwa die Kommandozeile verwenden muss oder von Windows auf Linux wechseln möchte zu entmystifizieren.
