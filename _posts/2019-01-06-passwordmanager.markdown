---
layout: post
title:  "Warum sollte man einen Passwortmanager verwenden?"
date:   2019-01-06 14:40:00 +0100
categories: security
image: /assets/passwordmanager.jpg
---

![Passwordmanager]({{ page.image }})

Im Laufe der Jahre sammeln sich so manche Online-Accounts an, die alle ein Passwort verlangen. Zur Vergabe der Passwörter gibt es unterschiedliche Möglichkeiten, die alle ihre Vor- und Nachteile haben. Manche Menschen verwenden ein Passwort für alles, manche haben sich ein System überlegt und manche verwenden Passwortmanager. Ich möchte hier die unterschiedlichen Möglichkeiten beleuchten und berichten, warum ich seit kurzem statt einem System einen Passwortmanager verwende.

## Ein Passwort für alles

Das eine Passwort für alles hat den Vorteil, dass man sich nur ein Passwort merken muss. Man kann das beliebig kompliziert gestalten und damit hat sich die Sache erledigt. Es hat jedoch den großen Nachteil, dass wenn das Passwort bekannt wird, jeder, der das Passwort besitzt, auf alle Online-Accounts Zugriff hat. Dazu benötigt ein Angreifer zwar noch den Benutzername (oft die E-Mailadresse), dieser sollte ihm jedoch auch bekannt sein, wenn er das Passwort schon ergattern konnte. Weiterhin hat das eine Passwort für alles den Nachteil, dass unterschiedliche Anbieter unterschliche Kriterien an ein Passwort stellen und somit manchmal das eine Passwort vielleicht nicht funktioniert, weil es ein Zeichen enthält oder nicht enthält, das gefordert ist. Nicht zu vergessen ist auch, dass wenn bekannt wird, dass ein Passwort eines Account veröffentlicht wurde, muss man alle Passwörter aller Accounts ändern.

## Passwort mit System

Das Passwort mit System habe ich die letzten Jahre verwendet, weil es mir lange Zeit sicher schien aber zur Zeit bin ich mir da nicht mehr so sicher. Die Idee ist relativ einfach, denn man überlegt sich einfach ein sicheres Passwort und hält einen gewissen Teil, zum Beispiel das Ende, variabel. Würde jetzt beispielsweise lookslikematrix einen Account ermöglichen, würde man den variablen Teil der Passworts mit den ersten zwei oder letzten drei Buchstaben (also *lo* oder *rix*) füllen und hätte dadurch für alle Accounts ein unterschiedliches Passwort. Der Vorteil ist wiederum, dass man sich nur einen fixen Anteil merken muss und trotzdem unterschiedliche Passwörter erhält. Wird eines meiner Passwörter bekannt, kann ein Angreifer lediglich auf einen Account zugreifen und nicht direkt auf alle. Der Nachteil dabei ist, dass wenn ein Angreifer zwei Passwörter in die Hände bekommt, dann kann er relativ leicht das System erkennen und hat wiederum Zugriff auf alle Accounts.

## Passwort mit Passwortmanager

Die Nutzung eines Passwortmanagers, der offline arbeitet, hat, wie ich finde, einen ganz neuen Charme. Ist er einmal installiert, ermöglicht er es zufällige Passwörter zu generieren, die sich kein Mensch merken kann und so viele Zeichen enthalten können, wie man möchte und damit zumindest einige Angriffe zunichte machen. Passwortmanager werden in der Regel mit einem Masterpasswort gesichert, das man sich wiederum merken muss, um überhaupt Zugriff auf die generierten Passwörter zu erhalten. Womit wieder der Nachteil gilt, wenn das Passwort bekannt wird und ein Angreifer Zugriff zum Passwortmanager erhält, hat er zu allen Accounts Zugriff. Der Vorteil dabei ist jedoch, dass er, zumindest bei den offline Passwortmanagern, Zugriff auf das Endgeräte benötigt, auf dem der Passwortmanager installiert ist. Es gibt auch online Passwortmanager, von denen ich jedoch nach meinem Empfinden die Finger weg lassen würde, weil man nie zu 100% sicher gehen kann, was mit den Daten passiert und wie sicher die Verbindung zum Anbieter ist. Ein weiterer Nachteil ist, dass wenn man das Passwort zum Passwortmanager vergisst, kein Zugriff auf die Passwörter mehr möglich ist. Daher muss man sich das Passwort wirklich gut merken.

Ich setze daher seit kurzem [pass](https://www.passwordstore.org/) auf der Kommandozeile und als Ergänzung [qtpass](https://qtpass.org/) als grafische Benutzeroberfläche ein. Mein bestehendes System behalte ich jedoch bei und werde lediglich langsam zum Passwortmanager wechseln. Ich werde zur Einrichtung von pass noch einen weiteren Blog-Eintrag schreiben. Es ist nämlich nicht nur Kommandozeilentool, sondern lässt sich auch auf dem Smartphone nutzen und über ein git-Repository sicher verteilen. Solltet ihr Fragen oder Anregungen haben, dann schreibt mir gerne eine Mail.
