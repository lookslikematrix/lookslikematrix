---
layout: post
title:  "PiCamera – Wie nehme ich ein Bild mit der PiCamera auf?"
date:   2019-06-23 13:40:00 +0200
categories: raspberry-pi
image: /assets/picamera.png
---

![PiCamera]({{ page.image }})

In der Regel findet man schon genügend Anleitungen, wie man mit Hilfe des Raspberry Pis und einer PiCamera Bilder aufnehmen kann. Und dennoch war es für meinen Anwendungfall nicht möglich eine Lösung zu finden. Ich wollte mit der *capture_continuous*-Methode Bilder aufnehmen ohne diese in einer *for-in*-Schleife zu verwenden. Dies kann beispielsweise notwendig sein, wenn ihr eine Klasse schreiben möchtet, die Bilder aufnehmen kann ohne jedesmal die Kamera erneut schließen und wieder öffnen zu müssen.

Um das zu erreichen, könnt ihr folgenden *Python*-Programmcode verwenden.
~~~python
from picamera import PiCamera

camera = PiCamera(resolution=(3296,2464))
camera_generator = camera.capture_continuous("test.jpeg", format="jpeg")

image = next(camera_generator)

camera_generator.close()
camera.close()
~~~

Was man dazu verstehen muss, ist was ein Generator in *Python* ist. Ein Generator in einer *for-in*-Schleife liefert in jedem Schleifendurchlauf ein neu generiertes Element. Am Beispiel der *capture_continuous*-Methode wird nach jedem Durchlauf ein neues Bild aufgenommen. Wenn man sich also nicht in einer *for-in*-Schleife befindet, dann muss man über die *next()*-Methode die Aufnahme triggern und kann dann anschließend mit der Aufnahme arbeiten. So kann dann die Verbindung zu Kamera offen gehalten werden und sauber am Ende geschlossen werden.

Ich hoffe meine kleine Erkenntnis bringt den ein oder anderen auch etwas weiter. Wenn ihr Fragen oder Anregungen habt, dann schreibt einfach ein Kommentar oder kontaktiert mich.
