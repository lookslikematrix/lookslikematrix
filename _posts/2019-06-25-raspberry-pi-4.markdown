---
layout: post
title:  "Raspberry Pi 4 – Warum man nie genügend Raspberry Pis zu Hause haben kann?"
date:   2019-06-25 17:10:00 +0200
categories: raspberry-pi
image: /assets/raspberrypi4.png
---

![Raspberry Pi 4]({{ page.image }})

Gestern wurde der neue **Raspberry Pi 4** veröffentlicht und ich habe mir natürlich direkt einen bestellt. Warum ich ihn direkt bestellt habe, warum ihr ihn bestellen solltet und warum man nie genügend Raspberry Pis zu Hause haben kann, erkläre ich euch.

Ich habe mir damals bereits den ersten Raspberry Pi bestellt, als ich noch keine Ahnung von Linux und von den Möglichkeiten eines Minicomputers hatte. Der Raspberry Pi war für mich der Weg in die OpenSource-Welt und mittlerweile verwende ich wenn irgendwie möglich OpenSource-Lösungen. Mit dem Raspberry Pi habe ich viel gelernt über Linux, Kommandozeilen-Tools und das es nicht immer ein fetter Server sein muss, sondern oft auch ein kleiner stromsparenden Rechner tut. Ich habe mir seit dem ersten Raspberry Pi fast jeden Raspberry Pi nach Hause geholt und war jedes Mal aufs neue fasziniert über die Leistungssteigerung. Die Raspberry Pis liegen dann auch nicht nur einfach bei mir in der Schublade herum, sondern drei davon sind tatsächlich im Dauerbetrieb. Einer steht bei Bekannten und ist für die Datensicherung über *rsync* zuständig, einer ermöglicht mir eine Hausautomatisierung mit *fhem* und der dritte ist mein Mediacenter mit *Kodi*. Während meines Studiums hatte ich sogar ein Raspberry Pi als Desktop-Rechner, weil PDFs lesen auch mit dem Raspberry Pi geht und mein richtiger Desktop-Rechner im Hochsommer einfach das Zimmer zu stark aufgeheizt hat.

Der neue **Raspberry Pi 4** liefert jetzt mit 1,5 GHz, bis zu 4 GB RAM, (richtigem) Gigabit-Ethernet, USB 3.0 und 2 Micro-HDMI Ausgängen mit bis zu 4K nochmals völlig neue Möglichkeiten. Und dabei hat sich der Preis für die 1 GB Variante nicht erhöht. Durch USB 3.0 und Gigabit-Ethernet lassen sich jetzt deutlich performantere Clouds mit Seafile oder Nextcloud realisieren, mit 2 HDMI Ausgängen ist das Arbeiten mit 2 Bildschirmen möglich und mit der Ausgabe des Bilds in 4K lässt sich ein richtig schönes Mediacenter betreiben. Oft ist für die Lösungen kaum noch Detailwissen nötig, da die Dokumentation und Setups oft so gut sind, dass auch der Leihe schnell an das Ziel kommt und wenn es dann doch mal klemmt, stehen einem etliche Antworten im Internet zur Verfügung. Die Community von Raspberry Pi ist extrem groß und extrem hilfsbereit und dadurch hebt sich der Raspberry Pi auch für mich von den anderen Minicomputern ab. Denn nichts ist ärgerlicher als ein System, dass ich in ein paar Jahren nicht mehr gebrauchen kann, weil es niemand pflegt.

Alles in allem freue ich mich riesig auf den **Raspberry Pi 4** und werde sicherlich den ein oder anderen Blog-Eintrag mit ihm umsetzen. Vorallem bin ich gerade gespannt, wie die Leistungssteigerung die Bildverarbeitung mit der PiCamera beeinflusst. Wenn ihr Fragen oder Anregungen habt, dann schreibt mir einfach, ich versuche schnellst möglich zu antworten.
