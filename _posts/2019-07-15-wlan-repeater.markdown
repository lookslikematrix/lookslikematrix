---
layout: post
title:  "WLAN Repeater – Wie baue ich mir meinen eigenen WLAN Repeater mit dem Raspberry Pi?"
date:   2019-07-15 16:35:00 +0200
categories: raspberry-pi
image: /assets/wlanrepeater.png
---

![WLAN Repeater]({{ page.image }})

Ich wurde gefragt, ob es mit dem Raspberry Pi möglich ist einen WLAN Repeater zu bauen. Nach einer kurzen Recherche wurde ich natürlich fündig \[[1](https://raspberrypi.stackexchange.com/questions/89803/access-point-as-wifi-router-repeater-optional-with-bridge)\]. Dank des integrierten WLAN-Moduls des Raspberry Pi 3 und auch des Raspberry Pi 4 ist dies sogar ohne zusätzlichen WLAN USB-Adapter möglich. Ich führe die ganzen Befehle auf dem aktuellen Raspbian Buster (Debian 10) aus, wobei es auch mit Raspbian Stretch (Debian 9) funktioniert.

Ich möchte hier kurz ein Erläuterung geben. Zunächst bringen wir den Raspberry Pi auf den aktuellen Stand. Dazu öffnen wir ein Terminal und geben die folgenden Befehle ein. Dabei muss er entweder mit dem Ethernet-Kabel zum Internet verbunden sein oder mit dem WLAN.
~~~bash
sudo apt update
sudo apt upgrade -y
~~~

Jetzt installieren wir ein notwendiges Paket *hostapd*, dass wir für den WLAN Repeater benötigen und aktivieren dies. *hostapd* ist eine Software die uns die Konfiguration eines Access-Points beziehungsweise eines WLAN Repeaters ermöglicht.
~~~bash
sudo apt install -y hostapd
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
~~~

Anschließend werden die aktuellen Services für *networking* und *dhcpcd* deaktiviert, die Konfiguration des Netzwerk-Interfaces verschoben und eine Konfigurationsdatei angepasst.
~~~bash
sudo systemctl mask networking.service
sudo systemctl mask dhcpcd.service
sudo mv /etc/network/interfaces /etc/network/interfaces.bak
sudo sed -i '1i resolvconf=NO' /etc/resolvconf.conf
~~~

Nun wird der *systemd-networkd* und *systemd-resolved* aktiviert.
~~~bash
sudo systemctl enable systemd-networkd.service
sudo systemctl enable systemd-resolved.service
sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
~~~

Als nächstes finden wir den Channel unserer aktuellen WLAN-Verbindung heraus, damit wir den gleichen Channel in der *hostapd*-Konfiguration im nächsten Schritt verwenden. Dazu folgende Befehle eingeben.
~~~bash
sudo iwlist wlan0 scan | grep 'Frequency:2.4' | uniq -c
~~~

Weiterhin dürft ihr euch jetzt für einen WLAN-Namen (SSID) und Passwort (wpa_passphrase) entscheiden und zusammen mit der Channel Information von oben, das folgende Konfigurationfile anpassen. Wenn ihr *lookslikematrix* als WLAN-Namen behalten wollt, dann solltet ihr aber mindestens das *verySecretPassword* durch ein eigenes Passwort ersetzen. Solltet ihr nachträglich noch die Datei anpassen wollen, könnt ihr mit *sudo nano /etc/hostapd/hostapd.conf* nachträglich Änderungen machen und diese mit Strg + X, Y und Enter speichern.
~~~bash
sudo bash -c 'cat > /etc/hostapd/hostapd.conf <<EOF
interface=ap0
driver=nl80211
ssid=lookslikematrix
country_code=DE
hw_mode=g
channel=6
auth_algs=1
wpa=2
wpa_passphrase=verySecretPassword
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
EOF'
~~~

Nun müssen die Rechte der Datei noch angepasst werden. Und die Datei als Konfiguration des *hostapd* verwendet werden.
~~~bash
sudo chmod 600 /etc/hostapd/hostapd.conf
sudo sed -i 's/^#DAEMON_CONF=.*$/DAEMON_CONF="\/etc\/hostapd\/hostapd.conf"/' /etc/default/hostapd
~~~

Des weiteren muss im *hostapd-Service *After=network.target* deaktiviert werden, indem folgenden Befehl ausgeführt wird, aus *After=network.target* ein *#After=network.target* mit vorgestellter Raute (#) wird und schließlich über Strg + X, Y und Enter gespeichert wird.
~~~bash
sudo systemctl --full edit hostapd.service
~~~

Als nächstes fügen wir *hostapd* unser neu definiertes Interface *ap0* hinzu, indem wir zunächst den Service editieren.
~~~bash
sudo systemctl edit hostapd.service
~~~

Und anschließend fügen wir folgenden Inhalt hinzu und speichern mit Strg + X, Y und Enter.
~~~bash
[Unit]
Wants=wpa_supplicant@wlan0.service

[Service]
ExecStartPre=/sbin/iw dev wlan0 interface add ap0 type __ap
ExecStopPost=-/sbin/iw dev ap0 del
~~~

Damit unserer WLAN Repeater sich auch mit unserem WLAN verbindet, müssen wir folgende Datei anlegen. Dabei müsst ihr euren WLAN-Namen und Passwort einfügen, die ihr bis jetzt immer verwendet habt.
~~~bash
sudo bash -c 'cat >/etc/wpa_supplicant/wpa_supplicant-wlan0.conf <<EOF
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1

network={
    ssid="TestNet"
    psk="realyNotMyPassword"
    key_mgmt=WPA-PSK   # see ref (4)
}
EOF'
~~~

Da ich mich mit meinem Raspberry Pi bereits mit dem WLAN verbunden hatte, für das ich auch den Repeater einrichten möchte, konnte ich diesen Befehl verwenden, welcher einfach die alte auf die neue Konfiguration kopiert. Ansonsten könnt ihr einfach mit dem nächsten Schritt weiter machen.
~~~bash
sudo cp /etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
~~~

Schließlich müssen wir noch die Konfiguration aktivieren, und dazu im Voraus die Dateiberechtigungen anpassen und die alte Konfiguration deaktivieren.
~~~bash
sudo chmod 600 /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
sudo systemctl disable wpa_supplicant.service
sudo systemctl enable wpa_supplicant@wlan0.service
~~~

Dann müssen wir den *wpa_supplicant@wlan0.service* editieren.
~~~bash
sudo systemctl edit wpa_supplicant@wlan0.service
~~~

Einfach die folgenden Zeilen einfügen und mit Strg + X, Y und Enter speichern.
~~~bash
[Unit]
BindsTo=hostapd.service
After=hostapd.service

[Service]
ExecStartPost=/sbin/iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
ExecStopPost=-/sbin/iptables -t nat -D POSTROUTING -o wlan0 -j MASQUERADE
~~~

Jetzt noch die eine Konfiguration für *wlan0*.
~~~bash
sudo bash -c 'cat > /etc/systemd/network/08-wlan0.network <<EOF
[Match]
Name=wlan0
[Network]
IPForward=yes
# If you need a static ip address toggle commenting next three lines (example)
DHCP=yes
#Address=192.168.10.60/24
#Gateway=192.168.10.1
# Optional: if you want to connect to your own DNS server, set it here (example)
#DNS=192.168.10.10 8.8.8.8
EOF'
~~~

Und eine Konfiguration für *ap0* und wir haben es geschafft.
~~~bash
sudo bash -c 'cat > /etc/systemd/network/12-ap0.network <<EOF
[Match]
Name=ap0
[Network]
Address=192.168.4.1/24
DHCPServer=yes
[DHCPServer]
# If you want to connect to your own DNS server, set its ip address here
DNS=84.200.69.80 84.200.70.40
EOF'
~~~

Als aller letztes jetzt noch ein Neustart und euer WLAN Repeater sollte funktionieren.
~~~bash
sudo reboot
~~~

Ich hoffe die Anleitung hat euch weiter geholfen. Hier unten ist auch noch der Link zur originallen Anleitung. Wenn ihr Fragen oder Anregungen habt, dann schreibt mir gerne. Ich freue mich über jede Nachricht.

---
\[1\]: [https://raspberrypi.stackexchange.com/questions/89803/access-point-as-wifi-router-repeater-optional-with-bridge](https://raspberrypi.stackexchange.com/questions/89803/access-point-as-wifi-router-repeater-optional-with-bridge)
