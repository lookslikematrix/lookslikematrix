---
layout: post
title:  "MicroPython – Wie kann ich meinen Mikrocontroller (ESP32) in Python programmieren?"
date:   2019-09-12 11:00:00 +0200
categories: microcontroller
image: /assets/micropython.png
---

![MicroPython]({{ page.image }})

Zur Entwicklung von Mikrocontrollern kommt in der Regel die Programmiersprachen C oder C++ in Frage. Dabei kommen gerade Programmier-Neulinge mit Python schneller in Kontakt und schneller zum Ergebnis. Dank [MicroPython](http://micropython.org/) kann man relativ schnell auch Python3 Programmcode auf einem Mikrocontroller, wie dem *ESP32* oder *ESP8266* ausführen.

*MicroPython* ist eine schlanke und effiziente Implementierung von Python3 mit einem verkleinerten Funktionsumfang der Python Standardbibliotheken. Ich möchte hier erläutern, wie ihr auf einem *ESP32* Mikrocontroller Python3 Programmcode ausführen könnt. Ich verwende dafür aktuell ein Ubuntu 18.04 und ein *ESP32*-Board, dass ich über USB mit meinem Computer verbunden habe.

Als erstes benötigen wir ein Tool, um die MicroPython-Umgebung auf unser Board zu bekommen, wobei uns [esptool](https://github.com/espressif/esptool/) hilft. *esptool* können wir über *pip* installieren, welches ein Tool zur Installation von Python Paketen ist. Also müssen wir zunächst *pip* installieren und anschließend *esptool*. Dazu könnt ihr die folgenden Befehle in ein Terminal eingeben.
~~~bash
# install pip
sudo apt install python-pip -y
# upgrade pip
sudo pip install --upgrade pip
# install esptool
sudo pip install esptool
~~~

Als nächstes müsst ihr die aktuelle MicroPython Version für euer Mikrocontroller-Board herunterladen. Unter [http://micropython.org/download#esp32](http://micropython.org/download#esp32) findet ihr die vorcompilierten **.bin*-Dateien für das *ESP32*-Board. Im September 2019 ist das eine Version *1.11*.
~~~bash
# change directory to Downloads
cd ~/Downloads
# download MicroPython
wget http://micropython.org/resources/firmware/esp32-20190529-v1.11.bin
~~~

Jetzt wird mit *esptool* der Speicher des Boards geleert und anschließend MicroPython darauf geschrieben.
~~~bash
# erase flash
esptool.py --port /dev/ttyUSB0 erase_flash
# write MicroPython on the flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190529-v1.11.bin
~~~

Damit wir jetzt Python3 Programmcode auf dem *ESP32* ausführen können, benötigen wir *picocom*, ein Tool, um uns auf das Board zu verbinden. Dies installieren wir zunächst und verbinden uns anschließend auf das Board.
~~~bash
# install picocom
sudo apt install picocom -y
# connect to board
picocom /dev/ttyUSB0 -b115200
~~~

Jetzt seid ihr mit dem *ESP32*-Board verbunden und könnt Python3 Code ausführen. Anschließend könnt ihr *picocom* mit *Strg + A* und *Strg + Q* beenden.
~~~python
print("looks like matrix or what?")
~~~

Damit ihr ein richtiges Skript schreiben könnt, dass dann auf dem Board ausgeführt wird, könnt ihr [ampy](https://github.com/pycampers/ampy) verwenden, dass auch mit *pip* installiert werden kann. Mit *ampy* habt ihr die Möglichkeit auf das Dateisystem von MicroPython zuzugreifen.
~~~bash
# install ampy
sudo pip install adafruit-ampy
# list files
ampy --port /dev/ttyUSB0 ls
~~~

Wenn ihr jetzt auf eurem Computer eine Datei anlegt, dann könnt ihr diese mit *ampy* auf dem Board ausführen.
~~~bash
# create file test.py
echo 'print("looks like matrix or what?")' > test.py
# execute test.py on the board
ampy --port /dev/ttyUSB0 run test.py
~~~

Für MicroPython gibt es zwei spezielle Dateien – *boot.py* und *main.py*. Das *boot.py*-Skript wird beim Starten zuerst ausgeführt, wenn es vorhanden ist und anschließend das *main.py*-Skript. Um die speziellen Dateien anzulegen, könnt ihr einfach auf eurem Computer eine Datei *boot.py* und *main.py* anlegen und diese dann mit *ampy* auf das Board übertragen. Die Dateien werden dann wie beschrieben automatisch nach einem Neustart ausgeführt.
~~~bash
# put boot.py on the board
ampy --port /dev/ttyUSB0 put boot.py
# put main.py on the board
ampy --port /dev/ttyUSB0 put main.py
~~~

Jetzt könnt ihr mit dem Skripten auf dem *ESP32* loslegen. Unter [https://docs.micropython.org/en/latest/esp32/quickref.html](https://docs.micropython.org/en/latest/esp32/quickref.html) findet ihr noch Erläuterungen, wie ihr das WLAN-Modul des *ESP32* ansprechen könnt, Zugriff auf die GPIOs habt und vieles mehr. Ich hoffe ich konnte euch helfen und freue mich sehr über Anregungen und Fragen.
