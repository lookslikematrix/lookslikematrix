---
layout: post
title:  "VirtualBox – Wie erstelle ich eine virtuelle Maschine auf der Kommandozeile?"
date:   2020-03-21 06:00:00 +0200
categories: server
image: /assets/micropython.png
---

![VirtualBox]({{ page.image }})

Beim Erstellen von virtuellen Maschinen (VM) kommt in vielen Fällen *VirtualBox* zum Einsatz. Mit Hilfe der grafischen Benutzeroberfläche hat man mit wenigen Klicks eine VM eingerichtet. Doch was macht man, wenn man keine Oberfläche zur Verfügung hat. Bei der Installation von *VirtualBox* wird das Tool *VBoxManage* zur Steuerung von *VirtualBox* auf der Kommandozeile mit installiert. Mit diesem Tool kann alles gesteuert, eingestellt und abgefragt werden, wie in der grafischen Oberfläche auch. Im Folgenden will ich euch kurz erläutern, wie ihr eine virtuelle Maschine mit *VBoxManage* erstellt und startet.

Zunächst einmal müsst ihr *VirtualBox* installiert haben. Unter Ubuntu funktioniert das wie folgt.
~~~bash
sudo apt install virtualbox
~~~

Dann könnt ihr mit **\-\-version** die Installation von *VBoxManage* überprüfen, indem der folgende Befehl eine Versionsnummer ausgibt.
~~~bash
VBoxManage --version
~~~

Jetzt können wir mit dem Einrichten einer VM starten. Ich erstelle eine *Debian 10* VM. Dazu benötigen wir einen virtuellen Rechner, der eine Festplatte und Arbeitsspeicher hat und dann ein Betriebssystem installiert bekommt. Also erstellen wir zunächst die virtuelle Hardware. Es ist von Vorteil mit **VBoxManage list ostypes** das richtige Betriebssystem auszuwählen, weil dadurch gewisse Grundeinstellungen gemacht werden.
~~~bash
## create VM
VBoxManage createvm --name "lookslikematrix" --ostype "Debian_64" --register
## add RAM
VBoxManage modifyvm "lookslikematrix" --memory 4096
## create drive with 100gb
VBoxManage createhd --filename "lookslikematrix_drive.vdi" --size 100000
~~~

Jetzt müssen wir noch die erstellte Festplatte in die VM einbauen.
~~~bash
# add IDE Controller
VBoxManage storagectl "lookslikematrix" --name "IDE Controller" --add ide --controller PIIX4
# connect hdd drive
VBoxManage storageattach "lookslikematrix" --storagectl "IDE Controller" --port 0 --device 0 --type hdd --medium lookslikematrix_drive.vdi
~~~

Damit wir ein Betriebssystem installieren können, müssen wir dieses zunächst herunterladen und schließlich die virtuelle DVD in die VM einlegen. Ihr könnt hier einfach eine andere ISO-Datei herunterladen und die Befehle durch diese ersetzen, um eine andere VM zu erstellen.
~~~bash
# download Debian 10 Live
wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-10.3.0-amd64-gnome.iso
# insert DVD into VM
VBoxManage storageattach "lookslikematrix" --storagectl "IDE Controller" --port 0 --device 1 --type dvddrive --medium debian-live-10.3.0-amd64-gnome.iso
~~~

Da wir keine grafische Oberfläche zur Verfügung haben, müssen wir jetzt noch Remote Desktop aktivieren und können dann nachdem Starten der VM über einen RDP-Client auf diese zugreifen.
~~~bash
# enable virtual remote desktop
VBoxManage modifyvm "lookslikematrix" --vrde on
# start VM
VBoxHeadless --startvm "lookslikematrix"
~~~

Unter Debian/Ubuntu/Raspbian konnte ich dann einfach mit *rdesktop* auf die VM zugreifen und das Betriebssystem installieren. Ihr musst dazu lediglich die IP-Adresse des Servers auf dem ihr die VM gestartet habt unten einfügen.
~~~bash
# install rdesktop
sudo apt install rdesktop
# start rdesktop with IP adress of headless server
rdesktop 192.168.172.66
~~~

Als letzten Schritt jetzt noch die virtuelle DVD auswerfen und ihr habt es geschafft.
~~~bash
VBoxManage storageattach "lookslikematrix" --storagectl "IDE Controller" --port 0 --device 1 --type dvddrive --medium emptydrive
~~~

Wenn ihr die VM herunterfahren wollt, dann könnt ihr das mit diesem Befehl tun.
~~~bash
VBoxManage controlvm "lookslikematrix" poweroff
~~~

Ich hoffe ihr könnt jetzt mit eurem virtuellen Rechner arbeiten und habt viel Spaß damit. Wenn ihr noch Fragen oder Anregungen habt, dann freue ich mich sehr darüber.
