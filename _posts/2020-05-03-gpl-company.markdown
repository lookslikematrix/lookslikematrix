---
layout: post
title:  "GPL Company – Ihr habt eine Idee? Ich kann sie programmieren."
date:   2020-05-03 20:00:00 +0200
categories: stuff
image: /assets/gpl_company.png
---

![GPL Company]({{ page.image }})

Ihr habt eine Idee? Ich kann sie programmieren. Als Unternehmen oder auch als Privatperson möchte man durch die Verwendung von Software seine Prozesse optimieren, sein Leben vereinfachen oder sein Leben schöner gestalten. Nicht immer gibt es die passende Lösung für die eigenen Anforderungen und dann gilt es ein Programme zu schreiben. Nicht immer hat man dafür die notwendigen Programmierkenntnisse und wenn man einen Dienstleister damit beauftragt, sind die Kosten so hoch, dass es sich nicht lohnt. Ich hatte daher eine Idee, die euch helfen könnte.

Ich programmiere für euch die Software, die ihr benötigt als OpenSource-Software unter der *GPL*-Lizenz. Was ihr dafür machen müsst, ist eine E-Mail mit eurer Idee an [christian.decker@lookslikematrix.de](mailto:christian.decker@lookslikematrix.de) schreiben und dann überlege ich mir, ob ich euch helfen kann. Die *GPL*-Lizenz ist eine OpenSource Lizenz, die es allen ermöglicht, die Software einzusetzen, zu vertreiben und auch zu optimieren. Sie muss jedoch dabei immer unter der *GPL*-Lizenz bleiben. Für Details verlinke ich hier mal den Lizenztext: [https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html).

Da ich letztlich meine Zeit nicht umsonst hergebe, stehen folgende Gedanken hinter der Idee. Ihr bezahlt soviel ihr könnt beziehungsweise soviel wie euch die Idee wert ist. Ich darf durch die *GPL*-Lizenz die Software auch anderen Anforderungen anpassen und dadurch von anderen Personen Geld verlangen und wenn mal Service beim Installieren oder finden von Fehlern notwendig wird, kann ich dies auch berechnen. Ich bin mir allerdings sicher, dass man im Einzelfall, dann sowieso darüber spricht.

Ich freue mich über eure Ideen!
