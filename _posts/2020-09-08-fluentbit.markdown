---
layout: post
title:  "Fluent Bit – Wie kann ich Daten über meine Applikation sammeln?"
date:   2020-09-08 09:00:00 +0200
categories: programming
image: /assets/fluentbit.png
---

![fluentbit]({{ page.image }})

Das Sammeln von Daten über die Nutzung von einer Applikation kann aufschlussreiche Ergebnisse bringen. *Wie viele aktive Benutzer habe ich?*, *Welche Aktionen benötigen wie lange?* oder *Wird mein neues Feature genutzt?*. Das sind oft Fragen, die mit einer Desktopapplikation schwierig zu beantworten sind. Ich möchte euch zeigen, wie ihr Daten einer Python-Applikation mit Hilfe von *Fluent Bit* sammeln könnt. *Fluent Bit* ist ein Datacollector/Datensammler, der aus mehreren Quellen Daten sammelt und diese in mehrere Datensenken speichern kann. In unserem Beispiel wird die Quelle eine Python-Applikation sein und die Datensenke eine Datei. Üblicherweise würde man als Datensenke eher eine Datenbank wie *elasticsearch* verwenden, jedoch würde das den Rahmen sprengen.

Als erstes benötigt ihr einen *Fluent Bit*-Server, der die Daten eurer Python-Applikation entgegen nehmen kann. Dazu verwende ich einen Raspberry Pi Zero mit dem aktuellen Raspberry Pi OS (Buster). Dort müsst ihr folgende Befehle ausführen, um *Fluent Bit* zu installieren.

~~~bash
sudo apt update
sudo apt upgrade -y

wget -qO - https://packages.fluentbit.io/fluentbit.key | sudo apt-key add -
sudo sed -i -e '$adeb https://packages.fluentbit.io/raspbian/buster buster main' /etc/apt/sources.list
sudo apt update
sudo apt install td-agent-bit -y
sudo service td-agent-bit start
~~~

Sollte alles funktioniert haben, könnt ihr mit **sudo service td-agent-bit status** den Status von *Fluent Bit* abfragen und mit **cat /var/log/syslog** sehen, dass in der Standardkonfiguration der Raspberry Pi die CPU Auslastung in */var/log/syslog* schreibt. Jetzt passen wir die Standardkonfiguration so an, dass diese Nachrichten aus dem Netzwerk entgegen nimmt und diese in eine Datei speichert. Dazu müsst ihr die Datei */etc/td-agent-bit/td-agent-bit.conf* öffnen und am Ende der Datei die **INPUT** und **OUTPUT** Einträge entfernen und durch die im folgenden Codeblock ersetzen. Anschließend noch bit *Strg + X* und *Enter* die Änderung bestätigen.

~~~bash
# open file
sudo nano /etc/td-agent-bit/td-agent-bit.conf

# delete INPUT and OUTPUT entry
# add this section
[INPUT]
    Name        forward
    Listen      0.0.0.0
    Port        24224
    Chunk_Size  32
    Buffer_Size 64

[OUTPUT]
    Name file
    Match *
    Path /home/pi/
~~~

Danach den *Fluent Bit*-Service mit **sudo service td-agent-bit restart** neu starten und ihr solltet Nachrichten von Applikationen aus dem Netzwerk entgegen nehmen können.

Damit jetzt eure Python-Applikation Nachrichten an den *Fluent Bit*-Server schickt müsst ihr zunächst mit **pip install fluent-logger** ein Python-Modul installieren und könnt dann im Anschluss direkt Nachrichten an den Server schicken. Hier ist ein kleine Beispiel, indem ihr eventuell den Hostname von *raspberrypi* auf euren Hostname ändern müsst und den Port, falls ihr diesen in der obigen Konfiguration anders vergeben habt.

~~~python
import logging
from fluent import handler

custom_format = {
  'host': '%(hostname)s',
  'type': '%(levelname)s'
}

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('fluent.lookslikematrix')
fluentHandler = handler.FluentHandler('lookslikematrix.follow', host='raspberrypi', port=24224)
formatter = handler.FluentRecordFormatter(custom_format)
fluentHandler.setFormatter(formatter)
logger.addHandler(fluentHandler)

logger.info("Lookslikematrix write info message to Fluent Bit server.")
~~~

Wenn ihr das Skript auf irgendeinem Rechner im Netzwerk ausführt, solltet ihr danach auf dem Raspberry Pi einen Datei unter */home/pi/lookslikematrix.follow* finden, die folgenden Inhalt hat.

~~~bash
lookslikematrix.follow: [1599399675.000000, {"host":"computerinthesamenetwork","type":"INFO","message":"Lookslikematrix write info message to Fluent Bit server."}]
~~~

Damit habt ihr es geschafft und könnt jetzt Informationen über euer Python-Applikation auf einem zentralen Server sammeln und diese dann analysieren. Ihr könnt die Informationen mit unterschiedlichsten Sachen noch anreichern, indem ihr im Beispiel die **custom_format**-Variable anpasst. [Hier](https://docs.python.org/3/library/logging.html#logrecord-attributes) findet ihr vordefinierte Attribute dafür, ihr könnt aber auch selbst Attribute definieren, die euch die Informationen liefert, die ihr benötigt.

Mit Hilfe des *Fluent Bit*-Server könnt ihr natürlich noch viel mehr Daten sammeln, die auch aus anderen Programmiersprachen und anderen Quellen kommen. Dazu könnt ihr einfach mal die Dokumentation von [*Fluent Bit*](https://docs.fluentbit.io/manual/pipeline/inputs) anschauen und aber auch von [*FluentD*](https://www.fluentd.org/datasources) dem "großen" Bruder von *Fluent Bit* . Die *FluentD* Module funktionieren aller auch mit *Fluent Bit*, wenn diese das *forward*-Protokoll implementiert haben. Ich hoffe der Blogeintrag hat euch geholfen und ich freue mich immer sehr über Fragen oder Anregungen.
