---
layout: post
title:  "OpenSource Blog – Warum diese Blog OpenSource ist?"
date:   2020-09-11 21:00:00 +0200
categories: stuff
image: /assets/lookslikematrix.png
---

![OpenSource Blog]({{ page.image }})

Ich habe seit einiger Zeit *lookslikematrix* als Open Source veröffentlicht und möchte euch hier kurz erläutern warum und wie ihr zu diesem Blog beitragen könnt.

Der Quellcode für *lookslikematrix* findet ihr unter [https://gitlab.com/lookslikematrix/lookslikematrix](https://gitlab.com/lookslikematrix/lookslikematrix). Dort findet ihr unter dem
*master*-Branch den aktuell veröffentlichten Blog. Ich habe den Quellcode zu *lookslikematrix* veröffentlicht, weil ich es nur für sinnvoll halte, dass jeder sich anschauen kann, wie so ein Blog mit [*jekyll*](https://jekyllrb.com/) erstellt werden kann und vor allem damit ihr die Möglichkeit habt Fehler zu beheben oder sogar eigene Beiträge zu erstellen.

Ich mache mit Sicherheit, den ein oder anderen Fehler und vielleicht sind bei älteren Beiträgen auch mittlerweile die Dinge überholt und sollten aktualisiert werden, daher fühlt euch frei einfache Verbesserungen oder neue Beiträge über *Merge Requests* auf *GitLab* einzureichen.

Ihr benötigt dazu *git* und einen Account bei *GitLab*, dann könnte ihr den Code herunterladen, das Repository unter eurem Benutzer *forken*, bearbeiten und dann eure Änderung über einen *Merge Request* in das Original-Repository hinzufügen.

Hier ein grobes Beispiel, was ihr auf der Kommandozeile machen müsst.

~~~bash
# clone repository and switch directory
git clone https://gitlab.com/lookslikematrix/lookslikematrix.git
cd lookslikematrix

# create fork on GitLab and add it as remote repository
git remote add fork git@gitlab.com:<EUERBENUTZERNAME>/lookslikematrix.git

# make changes in the repository
# create a commit
git add .
git commit -m 'fix some typos'
git push fork master

# create a merge request in GitLab and wait for me to accept your changes
~~~

Jetzt bin ich mal gespannt, wer der erste *Contributer* zu meinem Blog wird. Vielen Dank im Voraus.
