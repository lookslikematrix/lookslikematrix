---
layout: post
title:  "k3s – Wie starte ich eine Applikation in meinem Kubernetes Cluster?"
date:   2020-10-17 21:00:00 +0200
categories: raspberry-pi
image: /assets/k3s_gogs.png
---

![k3s & gogs]({{ page.image }})

Ich habe bereits [hier](https://lookslikematrix.de/raspberry-pi/2020/09/27/k3s.html) beschrieben, wie ihr mit *k3s* ein Kubernetes Cluster zu Hause betreibt. Jetzt möchte ich euch erklären, wie ihr in diesem Cluster eine Applikation betreibt. Ich nehme dafür einfach Mal [gogs](https://gogs.io/) – ein einfacher Git-Service zur Verwaltung eurer Projekte.

Zur Betreiben einer Applikation in eurem Kubernetes Cluster benötigt ihr drei Kubernetes Dinge (*Persistent Volume Claim*, *Deployment* und *Service*) und die Applikation sollte als Docker Image für *ARM* (*Raspberry Pi*) vorliegen.

Zunächst legen wir eine Festplatte in unserem Kubernetes Cluster an, worin *gogs* die Daten persistent speichern kann, daher der Name *Persistent Volume Claim*. Dazu müssen wir zunächst unserem Kubernetes Cluster beibringen, wie er diese Festplatte anlegen soll. Wir verwenden dafür einen *Local Path Provisioner*, der einfach lokal auf einem *Node* Speicherplatz freigibt. Dazu folgenden Befehl eingeben.

~~~bash
kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
~~~

Jetzt solltet ihr ein PVC (*Persistent Volume Claim*) anlegen können. Speichert dazu folgen Inhalt in eine Datei mit dem Namen *volumes.yaml* und führt anschließend **kubectl create -f volumes.yaml** aus.

~~~yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: gogs-pvc
  namespace: default
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: local-path
  resources:
    requests:
      storage: 2Gi
~~~

Wenn alles geklappt hat gibt **kubectl get pvc** folgende Ausgabe aus.

~~~bash
NAME       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
gogs-pvc   Bound    pvc-3197fecd-dd67-461d-99b9-0c3bb326a2ce   2Gi        RWO            local-path     23h
~~~

Anschließend werden wir *gogs* in unserem Cluster starten. Dazu speichern wir folgenden Inhalt in einer Datei *deployment.yaml* ab und führen folgenden Befehl aus **kubectl create -f deployment.yaml**.

~~~yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: gogs-deployment
  labels:
    app: gogs
spec:
  selector:
    matchLabels:
      app: gogs
  template:
    metadata:
      labels:
        app: gogs
    spec:
      containers:
      - name: gogs
        image: gogs/gogs-rpi:latest
        ports:
        - containerPort: 3000
        volumeMounts:
        - name: gogs-volume
          mountPath: /data
      volumes:
      - name: gogs-volume
        persistentVolumeClaim:
          claimName: gogs-pvc
~~~

Hat alles funktioniert bekommt ihr bei der Eingabe von folgendem Befehl **kubectl get deployment** diese Ausgabe, wobei hier *1/1* bei *READY* stehen sollte. Es kann auch sein, dass euer Pod noch kurz zum Starten benötigt bevor dieser hier angezeigt wird, da das Docker Image erst heruntergeladen werden muss und anschließend gestartet wird. Also müsst ihr hier eventuell kurz warten.

~~~bash
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
gogs-deployment   1/1     1            1           23h
~~~

Damit ihr euren *gog* Server jetzt von innerhalb eures Netzwerks erreichen könnt, müsst ihr noch einen Service anlegen. Dazu den folgenden Codeblock in einer Datei *services.yaml* speichern und dann mit **kubectl create -f service.yaml** ausführen.

~~~yaml
apiVersion: v1
kind: Service
metadata:
  name: gogs-service
spec:
  type: LoadBalancer
  selector:
    app: gogs
  ports:
    - protocol: TCP
      port: 7777
      targetPort: 3000
~~~

Hat das geklappt könnt ihr jetzt den Browser öffnen und unter http://\<hostname_k3s_server\>:7777 den *gog* Server erreichen und konfigurieren. Dank dem *PVC* könnt ihr jetzt zum Beispiel den *Pod* weglöschen und euer *Deployment* wird automatisch einen neuen *Pod* hochfahren mit den im *PVC* gespeicherten Daten. Ich habe dazu mein *gog* Server mit *SQLite* konfiguriert, ein Repository dort angelegt, mit Daten gefüllt und dann den *Pod* einfach weggelöscht. Danach habe ich kurz gewartet und konnte unter meiner URL den *gog* Server wieder erreichen.

Ich hoffe ich konnte euch einen kurzen Überblick geben, wie ihr in eurem Cluster ein Applikation starten könnt. Wenn ihr Fragen oder Anregungen habt, dann könnt ihr mir gerne eine Mail schreiben.
