---
layout: post
title:  "MicroPython – Wie kann ich MicroPython auf dem Raspberry Pi entwickeln?"
date:   2020-11-04 10:25:00 +0200
categories: raspberry-pi
image: /assets/micropython.png
---

![MicroPython]({{ page.image }})

Vor gut einem Jahr habe ich bereits über [*MicroPython*](https://lookslikematrix.de/microcontroller/2019/09/12/micropython.html) geschrieben, um damit das *ESP32*-Board zu programmieren. Ich hab damals schon gesehen, dass es auch eine Port von *MicroPython* für den **Raspberry Pi** gibt und habe mir jetzt mal angeschaut, wie das ganze funktioniert. Ihr fragt euch vielleicht warum man *MicroPython* und nicht *Python* auf dem Raspberry Pi entwickeln möchte. Eine möglich Antwort ist, weil es geht und eine andere weil ich somit Programmcode schreiben kann, der zum einen auf dem Raspberry Pi läuft als auch auf einem anderen Microcontroller-Board, dass *MicroPython* unterstützt.

Zunächst einmal müssen wir *MicroPython* installieren. Ich habe dazu folgende Anleitung gefunden \[[1](https://www.raspberrypi.org/forums/viewtopic.php?p=1456736)\] und etwas angepasst. Zunächst installieren wir notwendige Abhängigkeiten und laden *MicroPython* mit Hilfe von *git* herunter.

~~~bash
sudo apt install git build-essential libffi-dev -y
cd ~
git clone https://github.com/micropython/micropython.git
~~~

Als nächstes kompilieren wir *MicroPython*.

~~~bash
cd ~/micropython/ports/unix
make clean
make axtls
cd ~/micropython/mpy-cross
make
cd ~/micropython/ports/unix
make
~~~

Damit wir sicher gehen können, dass auch alles funktioniert, kompilieren wir noch die Tests und führen diese aus. Im Ergebnis sollten dann alle Tests (circa 780) erfolgreich durchgelaufen sein.

~~~bash
cd ~/micropython/ports/unix
make test
cd ~/micropython/tests
./run-tests
~~~

Jetzt legen wir noch einen symbolischen Link an, damit wir *MicroPython* von überall aus ausführen können.

~~~bash
sudo ln -s ~/micropython/ports/unix/micropython /usr/local/bin/micropython
~~~

Um nun ein eigenes Skript zu schreiben, könnt ihr beispielsweise eine LED an den GPIO Port 17 anschließen. Das Bild wurde mit [Fritzing](https://fritzing.org/) erstellt.

![Raspberry Pi mit LED](/assets/raspberry_pi_led.svg)

Anschließend kopiert ihr folgenden Codeblock in eine Datei mit dem Name *led.py*. Dazu mit *nano led.py* einen Editor öffnen, den Inhalt hinein kopieren und mit *Strg + X*, *Y* und *Enter* speichern.

~~~python
from utime import sleep

print("lookslikematrix LED")

GPIO_PATH = "/sys/class/gpio/"
EXPORT_PATH = GPIO_PATH + "export"
UNEXPORT_PATH = GPIO_PATH + "unexport"
GPIO_PORT = 17
GPIO_PORT_PATH = GPIO_PATH + "gpio" + str(GPIO_PORT) + "/"
DIRECTION_PATH = GPIO_PORT_PATH + "direction"
VALUE_PATH = GPIO_PORT_PATH + "value"
LOOP_COUNTER = 0

def set_gpio(file, value):
    file_handler = open(file, "w")
    file_handler.write(str(value))
    file_handler.close()

try:
    set_gpio(EXPORT_PATH, GPIO_PORT)
    set_gpio(DIRECTION_PATH, "out")
    while True:
         LOOP_COUNTER = LOOP_COUNTER + 1
         if LOOP_COUNTER == 10:
             break
         set_gpio(VALUE_PATH, 1)
         sleep(0.1)
         set_gpio(VALUE_PATH, 0)
         sleep(0.1)
except:
    print("An error occured!")
finally:
    set_gpio(UNEXPORT_PATH, GPIO_PORT)
~~~

Wenn ihr jetzt **sudo micropython led.py** eingebt, wird die LED zehnmal blinken und dann das Skript geschlossen, damit haben wir es geschafft, dass ihr *MicroPython* auf dem Raspberry Pi programmieren könnt. Ich hoffe ich konnte euch helfen. Wenn ihr Fragen oder Anregungen habt, dann könnt ihr mir gerne eine Mail schreiben. Wenn ihr mich unterstützen wollt, könnt ihr mir gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) kaufen.

---
\[1\]: [https://www.raspberrypi.org/forums/viewtopic.php?p=1456736](https://www.raspberrypi.org/forums/viewtopic.php?p=1456736)
