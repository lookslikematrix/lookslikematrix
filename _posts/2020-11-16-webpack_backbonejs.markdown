---
layout: post
title:  "Webpack – Wie erstelle ich ein JavaScript Bundle?"
date:   2020-11-16 21:25:00 +0200
categories: programming
image: /assets/webpack.png
---

![Webpack]({{ page.image }})

Bei der Entwicklung von Webapplikationen kommt in der Regel JavaScript zum Einsatz. Dazu gehört dann der ganze Kosmos mit JavaScript-Framework, CSS-Framework, Transpiler und vielem mehr und schnell raucht einem der Kopf, weil man gar nicht mehr versteht, wie das alles überhaupt funktioniert und zusammen spielt. Obendrauf will man den Programmcode, dann noch einfach ausliefern ohne jedesmal die HTML-Seite zu bearbeiten aber trotzdem nicht den Komfort von einer debuggbaren Applikation verlieren. An dieser Stelle kommt **Webpack** zum Einsatz. **Webpack** hilft einem Programmcode in einem *Bundle* zu bündeln und ermöglicht es durch seine etlichen Plugins JavaScript-Framework, CSS-Framework und Transpiler einzubinden und somit ein einfaches entwickeln und einfaches debuggen zu ermöglichen, sowie ein einfaches ausliefern eine JavaScript Webapplikation.

Ich habe mir dazu hauptsächlich den [Guide](https://webpack.js.org/guides/) von *Webpack* angeschaut und das ganze auf eine [*Backbone.js*](https://backbonejs.org/)-Applikation losgelassen. Im folgenden will ich euch erläutern, wie ihr am einfachsten Webpack konfiguriert. Als erstes legen wir uns einen Ordner an, indem wir unsere Applikation erstellen möchten, sowie notwendige Unterordner.

~~~bash
mkdir lookslikematrix
mkdir lookslikematrix/dist
mkdir lookslikematrix/src
mkdir lookslikematrix/src/views
~~~

Nun installieren wir noch *nodejs*, wo ihr [hier](https://nodejs.org/en/), die Installationsanweisungen findet. Unter Debian sind es folgenden Befehle. Die Installation von *nodejs* bringt automatisch das Tool *npm* mit, womit ihr dann die Pakete wie *Webpack* und *Backbone.js* installieren könnt.

~~~bash
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
~~~

Danach initialisieren wir eine *nodejs*-Projekt mit **npm init**. Nach Eingabe des Befehls kann man alle Fragen mit Enter bestätigen und schließlich mit *Y* die Initialisierung abschließen. Dann können alle Abhängigkeiten installiert werden.

~~~bash
npm install webpack webpack-cli webpack-dev-server --save-dev
npm install backbone backbone.localstorage jquery --save
~~~

Jetzt starten wir die Konfiguration von *Webpack*. Da wir während der Entwicklung möchten, dass die Webapplikation automatisch aktualisiert wird, wenn Programmcode geschrieben wird, verwende wir den installierten *webpack-dev-server* und füge der JSON-Konfiguration in *package.json* unter *script* **"start": "webpack server"** hinzu, damit es wie folgt aussieht.

~~~json
"scripts": {
  "test": "echo \"Error: no test specified\" && exit 1",
  "start": "webpack serve"
}
~~~

Anschließend erstellen wir noch eine Datei *webpack.config.js* mit folgendem Inhalt, die die Konfiguration für *Webpack* definiert. Also, dass wir im Entwicklungsmodus sind, das der Einstiegunspunkt unsere Applikation *./src/app.js* ist, das unser Applikation unter der Datei *bundle.js* gebündelt wird und das unser *webpack-dev-server*, den Inhalt des *dist*-Verzeichnis betrachten soll.

~~~js
const path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/app.js',
  devServer: {
    contentBase: './dist',
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
~~~

Wenn ihr jetzt **npm run start** ausführt, werdet ihr feststellen, dass ihr einen Fehler bekommt, da die Datei *./src/app.js* nicht existiert. Also erstellen wir diese und werden dann sehen, dass *Webpack* uns mit den Worten *Compiled successfully* erfreut. Jetzt liegt es eigentlich nur noch daran die *Backbone.js*-Applikation zu erstellen. Dazu erstellen wir zunächst eine *index.html*-Datei im *dist*-Verzeichnis mit folgendem Inhalt. Danach könnt ihr unter [http://localhost:8080](http://localhost:8080) einen Browser öffnen und die Entstehung eurer Applikation betrachten. In der *index.html*-Datei findet ihr auch die *bundle.js*-Datei aus der *Webpack*-Konfiguration wieder, die *Webpack* generiert.

~~~html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Lookslikematrix</title>
    </head>
    <body>
        <section class="app">
            <section class="main">
            </section>
        </section>
        <script src="bundle.js"></script>
    </body>
</html>
~~~

Als nächstes wollen wir die *app.js*-Datei mit Leben füllen und kopieren folgenden Programmblock hinein.

~~~js
import $ from 'jquery';
import AppView from './views/app-view';

$(() => {
    new AppView().render();
})
~~~

Und damit wir jetzt auch auch mal Inhalte auf unsere Seite angezeigt bekommen, erstellen wir die in der *app.js*-Datei referenzierte *AppView* in der Datei *app-view.js*.

~~~js
import { View } from 'backbone';

export default View.extend({
    el: '.app',

    initialize: function() {
        this.$main = this.$('.main');
    },

    render: function() {
        this.$main.html('<p>Lookslikematrix is awesome!</p>');
    },

    addOne: function (item) {
        console.log(item);
    },
})
~~~

An dieser Stelle könnt ihr jetzt eure *Backbone.js*-Applikation weiter aufsetzen und *Models*, *Collections* und *Views* anlegen, wie ihr Lust habt und *Webpack* wird alles in die *bundle.js* packen und ihr könnt einfach entwickeln. Ich hoffe ich konnte euch ein bisschen helfen und freue mich immer über Anregungen und Fragen über Mail. Für ein umfangreicheres Verständnis von Webpack, kann ich noch die offizielle Dokumentation [hier](https://webpack.js.org/concepts/) empfehlen. Wenn ihr mich unterstützen wollt, könnt ihr mir gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) kaufen.
