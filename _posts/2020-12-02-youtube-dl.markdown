---
layout: post
title:  "youtube-dl – Wie kann ich YouTube Videos herunterladen?"
date:   2020-12-02 21:40:00 +0200
categories: tools
image: /assets/youtube-dl.png
---

![youtube-dl]({{ page.image }})

Ab und zu möchte man ein YouTube-Video herunterladen, um es beispielsweise offline anschauen zu können. Manchmal möchte man ein YouTube-Video auch in einem anderen Dateiformat haben und manchmal braucht man nur die Tonspur eines Videos. Für die Anforderung gibt es viele Tools, die einem scheinbar helfen können, ich habe aber bis jetzt kein Tool gefunden, dass mich zufrieden gestellt hat. Mit [**youtube-dl**](http://youtube-dl.org/) habe ich ein Tool gefunden, dass all meine Anforderungen erfüllt. Wer sich nicht scheut eine Kommandozeile zu bedienen, der ist mit **youtube-dl** gut aufgehoben und kommt schnell an sein Ziel. **youtube-dl** bietet dabei nicht nur die Möglichkeit YouTube-Videos herunterzuladen, sondern von sehr vielen anderen Anbietern (siehe https://github.com/ytdl-org/youtube-dl/tree/master/youtube_dl/extractor).

Ich möchte hier kurz erläutern, was ihr machen müsst, damit ihr **youtube-dl** verwenden könnt und was für mich die Basisfunktionen sind. Für alles andere könnt ihr in der [Dokumentation](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme) lesen und etliche andere Anwendungsfälle abdecken.

Zunächst einmal solltet ihr Python3 auf eurem System installieren, sowie *pip*. Unter Debian/Ubuntu könnt ihr das wie folgt erreichen:

~~~bash
sudo apt install python3 python3-pip -y
~~~

Solltet ihr Windows verwenden empfehle ich die Installation über [*choco*](https://chocolatey.org/install) in der PowerShell als Admin oder über die Anweisungen auf der Python-Homepage [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/).

~~~powershell
choco install python3
~~~

Danach könnt ihr **youtube-dl** über *pip* installieren, indem ihr folgenden Befehl ausführt:

~~~bash
# Debian/Ubuntu
sudo -H pip3 install --upgrade youtube-dl
# Windows
pip install --upgrade youtube-dl
~~~

Jetzt kann es losgehen. Der einfachste Fall ist es einfach einen YouTube-Link zu nehmen und damit **youtube-dl** auszuführen. Ich nehme als Beispiel das *Big Buck Bunny*-Video, was unter der URL https://www.youtube.com/watch?v=aqz-KE-bpKQ erreichbar ist. Der Befehl sieht dann wie folgt aus:

~~~bash
youtube-dl https://www.youtube.com/watch?v=aqz-KE-bpKQ
~~~

Jetzt solltet ihr indem Verzeichnis, indem ihr euch in der Kommandozeile befindet, das heruntergeladen Video haben. Wenn ihr ein bisschen mehr entscheiden wollt, dann könnt ihr hier weiter machen. Ihr könnt nämlich nicht nur einfach das Video herunterladen, sondern auch entscheiden in welcher Qualität ihr es haben möchtet. Dazu könnt ihr *list-formats* verwenden.

~~~bash
youtube-dl https://www.youtube.com/watch?v=aqz-KE-bpKQ --list-formats
~~~

Ihr solltet dann für das angegebene Video diese Liste erhalten und könnt dann mit dem *format code* das entsprechende Format herunterladen. Möchte ich also das Video in der Auflösung *3840x2160* herunterladen, dann merkt ihr euch den *format code* 315.

~~~bash
format code  extension  resolution note
249          webm       audio only tiny   57k , opus @ 50k (48000Hz), 3.75MiB
250          webm       audio only tiny   75k , opus @ 70k (48000Hz), 4.95MiB
140          m4a        audio only tiny  130k , m4a_dash container, mp4a.40.2@128k (44100Hz), 9.80MiB
251          webm       audio only tiny  143k , opus @160k (48000Hz), 9.74MiB
256          m4a        audio only tiny  196k , m4a_dash container, mp4a.40.5 (24000Hz), 14.76MiB
258          m4a        audio only tiny  390k , m4a_dash container, mp4a.40.2 (48000Hz), 29.34MiB
394          mp4        256x144    144p   80k , av01.0.00M.08, 30fps, video only, 4.97MiB
278          webm       256x144    144p  100k , webm container, vp9, 30fps, video only, 6.48MiB
160          mp4        256x144    144p  115k , avc1.4d400c, 30fps, video only, 4.72MiB
395          mp4        426x240    240p  170k , av01.0.00M.08, 30fps, video only, 9.56MiB
242          webm       426x240    240p  231k , vp9, 30fps, video only, 13.23MiB
133          mp4        426x240    240p  254k , avc1.4d4015, 30fps, video only, 9.32MiB
396          mp4        640x360    360p  359k , av01.0.01M.08, 30fps, video only, 19.00MiB
243          webm       640x360    360p  424k , vp9, 30fps, video only, 24.49MiB
397          mp4        854x480    480p  663k , av01.0.04M.08, 30fps, video only, 33.42MiB
134          mp4        640x360    360p  723k , avc1.4d401e, 30fps, video only, 24.87MiB
244          webm       854x480    480p  769k , vp9, 30fps, video only, 41.49MiB
135          mp4        854x480    480p 1267k , avc1.4d401f, 30fps, video only, 47.84MiB
247          webm       1280x720   720p 1545k , vp9, 30fps, video only, 80.64MiB
398          mp4        1280x720   720p60 1891k , av01.0.08M.08, 60fps, video only, 94.36MiB
136          mp4        1280x720   720p 2584k , avc1.4d401f, 30fps, video only, 91.64MiB
302          webm       1280x720   720p60 2683k , vp9, 60fps, video only, 137.14MiB
399          mp4        1920x1080  1080p60 3572k , av01.0.09M.08, 60fps, video only, 167.57MiB
298          mp4        1280x720   720p60 4041k , avc1.4d4020, 60fps, video only, 136.16MiB
303          webm       1920x1080  1080p60 4469k , vp9, 60fps, video only, 228.17MiB
299          mp4        1920x1080  1080p60 6763k , avc1.64002a, 60fps, video only, 251.41MiB
308          webm       2560x1440  1440p60 13381k , vp9, 60fps, video only, 598.03MiB
315          webm       3840x2160  2160p60 26416k , vp9, 60fps, video only, 1.53GiB
18           mp4        640x360    360p  599k , avc1.42001E, 30fps, mp4a.40.2@ 96k (44100Hz), 45.32MiB
22           mp4        1280x720   720p 1340k , avc1.64001F, 30fps, mp4a.40.2@192k (44100Hz) (best)
~~~

Um das Video in der Auflösung *3840x2160* herunterzuladen, könnt ihr dann den folgenden Befehl verwenden:

~~~bash
youtube-dl https://www.youtube.com/watch?v=aqz-KE-bpKQ --format 315
~~~

Habt ihr jetzt noch die Notwendigkeit, dass ihr das Video in einem anderen Dateiformat benötigt, dann könnt ihr mit *ffmpeg*, dass Video herunterladen und anschließend direkt in das gewünschte Format wandeln. Dazu müsst ihr zunächst *ffmpeg* installieren.

~~~bash
# Debian/Ubuntu
sudo apt install ffmpeg
# Windows
choco install ffmpeg
~~~

Wenn ihr jetzt also beispielsweise eine *avi*-Datei benötigt, dann geht das mit *recode-video* wie folgt. Ihr könnt natürlich auch die Befehle kombinieren und somit ein bestimmtes Auflösung herunterladen und diese in ein bestimmtes Dateiformat wandeln.

~~~bash
youtube-dl https://www.youtube.com/watch?v=aqz-KE-bpKQ --format 22 --recode-video avi
~~~

Wenn ihr nur die Tonspur herunterladen möchtet, dann kann euch auch *ffmpeg* helfen. Dabei könnt ihr die Audioausgabe in unterschiedlichen Dateiformaten machen, wobei ich hier *mp3* nehme.

~~~bash
youtube-dl https://www.youtube.com/watch?v=aqz-KE-bpKQ --format 22 --extract-audio --audio-format mp3
~~~

Damit ist der kurze Einblick in die Möglichkeiten von **youtube-dl** vorbei. Ich kann nur empfehlen, dass ihr es auch mal mit anderen Portalen probiert, da es wirklich nicht nur auf YouTube begrenzt ist. Ich hoffe ich konnte euch helfen und freue mich wie immer über eure Fragen und Anregungen als Mail oder als Kommentar. Wenn ihr mich unterstützen wollt, könnt ihr mir gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) kaufen.
