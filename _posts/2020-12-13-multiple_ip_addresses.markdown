---
layout: post
title:  "Netzwerk – Wie kann ich meinem Computer mehrere IP-Adressen zuweisen?"
date:   2020-12-13 15:30:00 +0200
categories: server
image: /assets/Netzwerk.png
---

![Netzwerk]({{ page.image }})

Für meinen lokalen Server will ich alles (Versionsverwaltung, Kalender, Kontakte, Hausautomatisierung, etc.) auf ein Raspberry Pi Cluster mit **k3s** installieren. Damit ich besser den Überblick behalte, möchte ich weiterhin, dass die einzelnen Services unter sprechenden Namen im Netzwerk erreichbar sind, wozu ich ein DNS-Server benötige, der auch in dem Cluster installiert sein soll. Ich möchte also den Cluster und den DNS-Server über eine eindeutige IP-Adresse im Netzwerk erreichen können und benötige dafür zwei eindeutige IP-Adressen.

Die Anpassungen im Raspberry Pi sind dafür relativ gering und ich will euch eine kurze Schritt für Schritt Anleitung geben. Zunächst müsst ihr herausfinden, welche IP-Adressen ihr vergeben könnt. Ihr könnt dazu herausfinden, welche IP-Adresse euer Raspberry Pi hat. Wenn ihr euch auf dem Raspberry Pi befindet könnt ihr mit **ifconfig eth0** die aktuelle IP-Adresse des Ethernet-Anschluss erhalten (Das gleiche geht natürlich auch mit dem WLAN-Anschluss *wlan0*). Ihr bekommt dann die folgende Ausgabe, wobei die *192.168.178.40* eure IP-Adresse ist:

~~~bash
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.178.40  netmask 255.255.255.0  broadcast 192.168.178.255
        inet6 fe80::e539:bd1e:618a:df75  prefixlen 64  scopeid 0x20<link>
        inet6 2003:d5:f38:7600:2149:7de5:e60:e738  prefixlen 64  scopeid 0x0<global>
        ether b8:27:eb:f5:c0:ba  txqueuelen 1000  (Ethernet)
        RX packets 10659881  bytes 9788481129 (9.1 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1599448  bytes 189478776 (180.7 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
~~~

Ihr könnt jetzt vermuten, dass die Adresse *192.168.178.41* noch nicht belegt ist und mit **ping 192.168.178.41** prüfen, ob auch wirklich keine Antwort von der Adresse kommt. Ansonst einfach hinten hochzählen und den Vorgang wiederholen, bis der **ping**-Befehl keine Antwort gibt.

Mit der gefunden IP-Adresse könnt ihr dann eine Datei in */etc/network/interfaces.d/lookslikematrix_multiple_ip_addresses* anlegen. Dazu mit **sudo nano /etc/network/interfaces.d/lookslikematrix_multiple_ip_addresses** die Datei öffnen und den folgenden Inhalt hineinkopieren und gegebenfalls anpassen. *netmask* könnt ihr auch der obigen Ausgabe entnehmen und das Gateway eurer IP-Adresse anpassen. Zum Speichern müsst ihr noch *Strg + X*, *Enter* und *Y* eingeben.

~~~bash
auto eth0:1
allow-hotplug eth0:1
iface eth0:1 inet static
    vlan-raw-device eth0
    address 192.168.178.41
    netmask 255.255.255.0
    gateway 192.168.0.1
~~~

Wenn ihr jetzt nicht direkt mit dem Raspberry Pi verbunden seit (Bildschirm und Tastatur direkt verbunden sind), dann müsst ihr den Raspberry Pi noch mit **sudo reboot** neustarten, sonst könnt ihr mit **sudo ip link set eth0 down** und **sudo ip link set eth0 up** das Interface neustarten und sollten dann mit dem Befehl **ifconfig** folgenden Ausgabe erhalten.

~~~bash
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.178.40  netmask 255.255.255.0  broadcast 192.168.178.255
        inet6 fe80::e539:bd1e:618a:df75  prefixlen 64  scopeid 0x20<link>
        inet6 2003:d5:f38:7600:2149:7de5:e60:e738  prefixlen 64  scopeid 0x0<global>
        ether b8:27:eb:f5:c0:ba  txqueuelen 1000  (Ethernet)
        RX packets 10659881  bytes 9788481129 (9.1 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1599448  bytes 189478776 (180.7 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth0:1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.178.41  netmask 255.255.255.0  broadcast 192.168.178.255
        ether dc:a6:32:04:65:5d  txqueuelen 1000  (Ethernet)
~~~

Ihr müsstet jetzt sowohl mit 192.168.178.40 als auch mit 192.168.178.41 auf den Raspberry Pi zugreifen können. So könnt ihr auch noch mehr als zwei IP-Adressen dem Raspberry Pi zuweisen, indem ihr einfach die *lookslikematrix_multiple_ip_addresses* erweitert und anstatt *eth0:1* *eth0:2* schreibt und die nächste freie IP-Adresse angebt. Ich hoffe euch hat die kleine Anleitung geholfen. Wenn ihr Fragen oder Anregungen habt, dann freue ich mich sehr über eure Nachrichten und sonst freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).
