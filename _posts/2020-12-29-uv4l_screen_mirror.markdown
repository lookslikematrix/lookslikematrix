---
layout: post
title:  "uv4l – Wie kann ich mein Computerbildschirm mit einem Fernseher und einem Raspberry Pi teilen?"
date:   2020-12-29 14:00:00 +0200
categories: raspberry-pi
image: /assets/uv4l.png
---

![uv4l]({{ page.image }})

Ich wurde gefragt, ob es möglich ist den Computerbildschirm mit einem Fernseher zu teilen an dem ein *Raspberry Pi 1* angeschlossen ist. Auf der Suche nach einer Lösung wurde ich fündig \[[1](https://www.linux-projects.org/uv4l/tutorials/screen-mirroring/)\]. Ich kann vorneweg sagen, dass die Performance mit einem *Raspberry Pi 1* nicht akzeptabel ist, jedoch mit dem *Raspberry Pi 4* wirklich gut funktioniert. Die Lösung heißt **uv4l** und ist ein Tool, dass ihr auf dem *Raspberry Pi* installiert und dort einen Webserver zur Verfügung stellt. Über den Webserver könnt ihr dann auf eurem normalen Computer (Windows, Mac oder Linux) oder an eurem Smartphone im Browser eine Webseite aufrufen und dorthin den gewünschten Inhalt teilen. Auf dem angeschlossenen Bildschirm des *Raspberry Pi* wird dann der Inhalt dargestellt.

Im Folgenden erkläre ich kurz, was ihr machen müsst, damit das auch bei euch funktioniert. Ich verwende hierfür den *Raspberry Pi 4* mit einem aktuellen *Raspberry Pi OS Lite (Buster)*. Zunächst bringen wir den *Raspberry Pi* auf den aktuellen Stand.

~~~bash
sudo apt update
sudo apt upgrade -y
~~~

Anschließend installieren wir **uv4l** mit den notwendigen Abhängigkeiten, um den Bildschirm teilen zu können. Wenn ihr es auf dem *'Raspberry Pi 1* ausprobieren möchtet, dann müsst ihr im folgenden nicht *uv4l-webrtc* sondern *uv4l-webrtc-armv6* installieren.

~~~bash
curl https://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
echo "deb https://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" | sudo tee -a /etc/apt/sources.list.d/uv4l.list
sudo apt update
sudo apt install uv4l uv4l-dummy uv4l-server uv4l-webrtc
# for raspberry pi 1
# sudo apt install uv4l uv4l-dummy uv4l-server uv4l-webrtc-armv6
~~~

Da wir unseren Bildschirm über eine sichere Verbindung (*HTTPS*) teilen möchten, erstellen wir noch ein Zertifikat auf dem *Raspberry Pi*, dass wir dann in **uv4l** verwenden können. Dazu müsst ihr folgenden Befehl ausführen, die kommenden Fragen in der Kommandozeile ausfüllen und jeweils mit *Enter* bestätigen.

~~~bash
sudo bash -c "openssl genrsa -out /etc/ssl/private/selfsign.key 2048 && openssl req -new -x509 -key /etc/ssl/private/selfsign.key -out /etc/ssl/private/selfsign.crt -sha256"
~~~

Jetzt finden wir noch kurz unsere IP-Adresse mit *ifconfig* heraus und starten dann **uv4l** mit dem folgenden Befehl.

~~~bash
uv4l --driver dummy --auto-video_nr --enable-server --server-option '--use-ssl=yes' --server-option '--ssl-private-key-file=/etc/ssl/private/selfsign.key' --server-option '--ssl-certificate-file=/etc/ssl/private/selfsign.crt' --server-option '--enable-webrtc-video=no' --server-option '--enable-webrtc-audio=no' --server-option '--webrtc-receive-video=yes' --server-option '--webrtc-receive-audio=yes' --server-option '--webrtc-renderer-source-size=yes' --server-option '--webrtc-renderer-fullscreen=yes' --server-option '--webrtc-receive-datachannels=no' --server-option '--port=9000'
~~~

Daraufhin wechseln wir auf unseren Computer, öffnen Firefox und geben die folgenden URL ein. Ihr müsst natürlich die IP-Adresse durch eure tauschen.

~~~bash
https://192.168.178.49:9000/stream/webrtc
~~~

Ihr solltet dann den folgenden Inhalt sehen, wo ihr das selbst erstellte Zertifikat für die *HTTPS* Verschlüsselung akzeptieren müsst, indem ihr auf *Erweitert...* und anschließend auf *Risiko akzeptieren und fortfahren* klickt.

![Firefox Message](/assets/uv4l_selfsigned.png)

Als zusätzliche Sicherheit müsst ihr jetzt noch in der Firefox Konfiguration einen Eintrag hinzufügen. Dazu gebt ihr in der Adresszeile *about:config* ein, bestätigt dort mit einem Klick auf *Risiko akzeptieren und fortfahren* und gebt in das Feld *Einstellungsname suchen* *media.getusermedia.screensharing.allowed_domains* ein. Anschließend wählt ihr dort *String* aus, klickt auf das Plus-Symbol, gebt die IP-Adresse des Raspberry Pi ein und bestätigt die Eingabe mit einem Klick auf den Hacken.

Mit dieser Einstellung könnt ihr wieder auf die Seite **https://192.168.178.49:9000/stream/webrtc** wechseln, dort unter *Cast local Audio/Video sources to remote peer* bei Video *screen* anklicken und dann auf das grüne *Call!*-Button drücken. Firefox wird euch dann fragen, welchen Bildschirminhalt ihr teilen möchtet und diesen dann nach eurer Auswahl und Bestätigung auf dem Bildschirm des Raspberry Pis darstellen.

Ich hoffe die kleine Anleitung konnte euch helfen. Wenn ihr Fragen oder Anregungen habt, dann lasst gerne einen Kommentar da und sonst freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).

---
\[1\]: [https://www.linux-projects.org/uv4l/tutorials/screen-mirroring/](https://www.linux-projects.org/uv4l/tutorials/screen-mirroring/)
