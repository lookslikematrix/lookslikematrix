---
layout: post
title:  "Zertifikate – Wie erstelle ich mein eigenes Root Zertifikat?"
date:   2021-01-30 16:25:00 +0200
categories: server
image: /assets/zertifikate.png
---

![Zertifikate]({{ page.image }})

Für mein [home-server](https://gitlab.com/lookslikematrix/home-server)-Projekt wollte ich die einzelnen Services über HTTPS verschlüsseln. Dafür muss man für jeden Service eigenen Zertifikate anlegen. Wenn man diesen Zertifikaten ein gemeinsamen Root/Wurzel-Zertifikat gibt, dann muss man seinem PC oder Smartphone nur dieses Root Zertifikat hinzufügen und PC oder Smartphone vertrauen automatisch allen Zertifikaten, die von diesem Root Zertifikat erstellt worden sind. So funktioniert das übrigens, stark vereinfacht, auch mit allen HTTPS verschlüsselten Webseiten.

Zur Erzeugung der Zertifikate müsst ihr zunächst *openssl* installieren. Ich mache das unter Debian wie folgt:

~~~bash
sudo apt install openssl -y
~~~

Anschließend erzeugen wir den Root-Schlüssel mit folgendem Befehl und vergeben ein Passwort, dass wir uns merken sollten, da dies immer notwendig wird zur Erzeugung von neuen Zertifikaten.

~~~bash
openssl genrsa -aes256 -out rootkey.pem 4096
~~~

Jetzt erstellen wir unser Root-Zertifikat. Hierbei werden wir nach dem obigen Passwort gefragt, sowie nach ein paar Informationen, die dann bei der Anzeige des Zertifikat beispielsweise im Browser zu sehen sind. Ihr könnt die Felder auch leer lassen und einfach durch Enter bestätigen.

~~~bash
openssl req -x509 -new -nodes -extensions v3_ca -key rootkey.pem -days 1024 -out rootcertificate.pem -sha512
~~~

Da wir jetzt schon ein Root-Zertifikat haben, können wir jetzt das Zertifikat für unseren Service anlegen, dass von unserem Root-Zertifikat erstellt wird. Zunächst brauchen wir wieder einen Schlüssel.

~~~bash
openssl genrsa -out servicekey.pem 4096
~~~

Und anschließend legt ihr ein Service Request an, wobei wieder Informationen abgefragt werden. Die Information zum FQDN (Fully qualified domain name) ist dabei dringend auszufüllen. In meinem Beispiel *service.lookslikematrix.de*, als der Name unter welchem ihr später das Zertifikat verwenden wollt. Die anderen Informationen könnt ihr (wenn ihr wollt) einfach mit Enter bestätigen. Das Passwort am Schluss lasst ihr leer.

~~~bash
openssl req -new -key servicekey.pem -out servicerequest.csr -sha512
~~~

Danach erstellen wir das Zertifikat für unseren Service, wobei wir wieder das Passwort unseres Root-Schlüssel benötigen und haben dann erfolgreich ein Service Zertifikat angelegt.

~~~bash
openssl x509 -req -in servicerequest.csr -CA rootcertificate.pem -CAkey rootkey.pem -CAcreateserial -out servicecertificate.pem -days 365 -sha512
~~~

Je nach Anwendungsfall müsst ihr jetzt unterschiedliche Dinge mit dem Zertifikat machen. Ich baue es in meinen [home-server](https://gitlab.com/lookslikematrix/home-server) ein und benötige daher noch die *base64* kodierte Version davon, dazu füge ich noch das Root Zertifikat dem Service Zertifikat hinzu und generiere, dann meine Strings, die ich verwenden kann.

~~~bash
cat rootcertificate.pem >> servicecertificate.pem
base64 servicecertificate.pem -w0
base64 servicekey.pem -w0
~~~

Ich hoffe ich konnte euch etwas helfen, wenn ihr noch Fragen oder Anregungen habt, dann lasst es mich gerne über die Kommentarfunktion wissen. Ansonsten freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).
