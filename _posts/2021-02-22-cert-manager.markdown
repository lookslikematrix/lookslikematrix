---
layout: post
title:  "cert-manager – Wie lasse ich meine eigenen Zertifikat für mein Cluster erstellen?"
date:   2021-02-22 22:25:00 +0200
categories: server
image: /assets/cert-manager.png
---

![cert-manager]({{ page.image }})

In dem [letzten Blogeintrag](https://lookslikematrix.de/server/2021/01/30/own-root-ca.html) habe ich beschrieben, wie man ein eigenes Root-Zertifikat anlegt und mit Hilfe von diesem dann andere Zertifikate anlegen kann, um somit meinen [home-server](https://gitlab.com/lookslikematrix/home-server) abzusichern. Ein paar Tage später bin ich über [cert-manager](https://cert-manager.io/) gestolpert, welcher unteranderem genau das kann und noch mehr. Der Vorteil des *cert-manager* ist jedoch, dass ich nur das Root-Zertifikat erzeugen muss, die restlichen Zertifikate werden automatisch erzeugt. Damit spare ich mir Zeit und reduziere mögliche Fehlerquellen. Es ist allerdings nicht zu vergessen, dass mit Hilfe des Root-Zertifikat auch potentielle Angreifer "vertrauensvolle" Zertifikate anlegen können und somit Man-in-the-middle-Attacken möglich sind, daher sollte das Root-Zertifikat stets unzugänglich für andere gespeichert werden.

Damit ihr den *cert-manager* installieren könnt, verwendet ihr am besten *helm*. Die Installation geht unter Debian wie folgt:

~~~bash
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
~~~

Anschließend könnt ihr ganz einfach den *cert-manager* installieren.
~~~bash
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.2.0 -set installCRDs=true
~~~

Mit dem *cert-manager* könnt ihr unterschiedlichste Zertifikate erzeugen (siehe [https://cert-manager.io/docs/configuration/](https://cert-manager.io/docs/configuration/)), wobei ich in diesem Fall [**CA**](https://cert-manager.io/docs/configuration/ca/) benötige und dort einen **Cluster-Issuer** anlegen werden, welcher mir Zertifikate für mein komplettes Cluster anlegen kann. Es gibt auch die Möglichkeit mit **CA** die Erstellung der Zertifikate pro Namespace zu erstellen. Damit ich jetzt den *cert-manager* konfigurieren kann, benötige ich zunächst ein Root-Zertifikat.

~~~bash
openssl genrsa -out rootkey.pem 4096
openssl req -x509 -new -nodes -extensions v3_ca -key rootkey.pem -days 1024 -out rootcertificate.pem -sha512
~~~

Und anschließend erzeuge ich eine Datei *cert-manager-secret.yaml*, wo ich mit **base64 rootcertificate.pem -w0** und **base64 rootkey.pem -w0** das Root-Zertifikat in die jeweilige Zeile eintragen kann.

~~~yml
apiVersion: v1
kind: Secret
metadata:
    name: ca-key-pair
    namespace: cert-manager
data:
    tls.crt: <insert output from `base64 rootcertificate.pem -w0`>
    tls.key: <insert output from `base64 rootkey.pem -w0`>
~~~

Jetzt noch den **CA-Issuer** in eine Datei *ca-issuer.yaml* anlegen und wir können den *cert-manager* gleich verwenden.

~~~yml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ca-issuer
  namespace: cert-manager
spec:
  ca:
    secretName: ca-key-pair
~~~

Abschließend noch das *Secret* und den *Issuer* dem Cluster hinzufügen.

~~~bash
kubectl apply -f cert-manager-secret.yaml
kubectl apply -f ca-issuer.yaml
~~~

Und jetzt könnt ihr beispielsweise in eurem *Ingress* den Eintrag **cert-manager.io/cluster-issuer: ca-issuer** zu den den **annotations** hinzufügen und es wird automatisch ein Zertifikat erzeugt. Dazu muss dort der **name: ca-issuer** übereinstimmen mit dem erzeugen Issuer und es muss ein **secretName** festgelegt werden, in welches der *cert-manager* das erzeugt Zertifikat speichern soll.

~~~yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  namespace: lookslikematrix
  name: lookslikematrix-ingress
  annotations:
    cert-manager.io/cluster-issuer: ca-issuer
spec:
  tls:
  - hosts:
      - service.lookslikematrix.local
    secretName: lookslikematrix-tls
  rules:
  - host: service.lookslikematrix.local
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: lookslikematrix-service
            port:
              name: service
~~~

Ich hoffe ich konnte euch mit dieser kleinen Anleitung helfen eure Services mit Hilfe des *cert-manager* abzusichern. Wenn ihr Fragen oder Anregungen habt, könnt ihr gerne ein Kommentar schreiben und ansonsten freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).
