---
layout: post
title:  "Debian – Wie installiere ich Debian (64 Bit) auf meinem Raspberry Pi 4?"
date:   2021-02-24 22:00:00 +0200
categories: server
image: /assets/debian.png
---

![Debian]({{ page.image }})

Da *Raspberry Pi* seit neuestem auch mal ungefragt Microsoft Repository zu seinem Betriebssystem hinzufügt \[[1](https://www.heise.de/news/Raspberry-Pi-OS-Zoff-um-Microsoft-Paketverzeichnisse-nach-Update-5050653.html)\], war ich auf der Suche nach einem alternativen Betriebssystem. Und was könnte besser sein als ein richtiges **Debian** auf dem *Raspberry Pi* zu installieren. Hinzukommt noch, dass es für die aktuellen *Raspberry Pi* Modelle 3 & 4 als 64 Bit-System kommt.

Die Projektwebsite findet ihr unter [https://raspi.debian.net/](https://raspi.debian.net/). Dort sind dann [Images](https://raspi.debian.net/daily-images/) für alle *Raspberry Pi* Modelle zu finden mit dem aktuellen Debian Buster (stable) und Debian Bullseye (testing). Das hier bereitgestellt **Debian** ist vergleichbar mit dem *Raspberry Pi OS Lite*, also ohne grafische Benutzeroberfläche und damit hauptsächlich für Server/Service Funktionen wie einem [home-server](https://gitlab.com/lookslikematrix/home-server).

Wenn ihr also beispielsweise für euren *Raspberry Pi 4* ein Debian Buster möchtet könnt ihr es wie folgt herunterladen.

~~~bash
wget https://raspi.debian.net/daily/raspi_4_buster.img.xz
~~~

Jetzt müsst ihr das Image nur noch auf eine SD-Karte spielen. Dazu könnt ihr entweder den [Raspberry Pi Imager](https://www.raspberrypi.org/software/) verwenden oder folgenden Codeblock.

Zuerst müssen wir die Bezeichnung der SD-Karte herausfinden. Dabei ist zu beachten, dass ihr sicher das richtige */dev/{YOUR_DEVICE}* auswählt, da ihr sonst das Image auf einen falschen Datenträger kopiert und somit dort Daten zerstört. [Hier](https://raspi.debian.net/find-your-sd/) auch eine kleine Anleitung zum finden der richtigen SD-Karte. Der oberste Eintrag (beispielsweise */dev/sdb*) nach dem *diff*-Befehl ist eure SD-Karte.

~~~bash
ls /dev > /tmp/before_sd
# insert SD-Card
ls /dev > /tmp/after_sd
diff /tmp/before_sd /tmp/after_sd
~~~

Und anschließend könnt ihr das Debian Buster Image auf die SD-Karte kopieren.

~~~bash
xzcat raspi_4_buster.img.xz | sudo dd of=/dev/{YOUR_DEVICE} bs=64k oflag=dsync status=progress
~~~

Jetzt nur noch die SD-Karten in den *Raspberry Pi 4* stecken und schon kann es los gehen. Ich hoffe die kurze Anleitung hat euch geholfen. Wenn ihr Fragen oder Anmerkungen habt, dann schreibt gerne ein Kommentar und ansonsten freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).

---
\[1\]: [https://www.heise.de/news/Raspberry-Pi-OS-Zoff-um-Microsoft-Paketverzeichnisse-nach-Update-5050653.html](https://www.heise.de/news/Raspberry-Pi-OS-Zoff-um-Microsoft-Paketverzeichnisse-nach-Update-5050653.html)
