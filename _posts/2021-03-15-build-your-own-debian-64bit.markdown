---
layout: post
title:  "Debian – Wie baue ich mein eigenes Debian für den Raspberry Pi?"
date:   2021-03-15 21:00:00 +0200
categories: raspberry-pi
image: /assets/debian.png
---

![Debian]({{ page.image }})

[Hier](https://lookslikematrix.de/server/2021/02/24/debian-raspberry-pi-64bit.html) habe ich beschrieben, wie man Debian auf seinem *Raspberry Pi* installieren kann. Wem das noch nicht genügt, wer sein eigenes Debian für den *Raspberry Pi* bauen möchte und dann noch Modifikation vornehmen möchte, dem möchte ich hier helfen. Das Tool das einem dabei hilft und mit dem auch die Images auf [https://raspi.debian.net/](https://raspi.debian.net/) gebaut werden, heißt [image-specs](https://salsa.debian.org/raspi-team/image-specs). Dahinter steckt im Grunde [vmdb2](https://gitlab.com/larswirzenius/vmdb2), welches ein Tool ist, um Debian Images zu erstellen. *image-specs* ist dann vorallem die Konfiguration, um für den *Raspberry Pi* Images zu bauen.

Damit ihr euer eigenes Debian-Image für euren Raspberry Pi bauen könnt, benötigen wir ein paar Abhängigkeiten. Unter Debian 10 könnt ihr die wie folgt installieren. Weiterhin ist ein 64 Bit System notwendig zur Erstellung der Images für den Raspberry Pi 3 & 4. Ihr könnt als nicht auf eurem *Raspberry Pi OS 32 Bit* das Debian 64 Bit erstellen (Ich hab das versucht).

~~~bash
sudo apt update
sudo apt -y upgrade
sudo apt install -y git dosfstools parted qemu-img qemu-utils qemu-user-static debootstrap binfmt-support time kpartx python3 python3-pip python3-jinja python3-yaml
~~~

Anschließend müssen wir *vmdb2* unter Debian 10 noch händisch installieren, da hier noch eine Version 0.13 installiert werden würde wir aber eine Version >= 0.17 benötigen. Unter Debian 11 braucht ihr diesen Schritt nicht machen, sondern lediglich mit **sudo apt install vmdb2** *vmdb2* installieren. Zur Installation laden wir uns das Git-Repository herunter und installieren *vmdb2* mit Hilfe von *pip*.

~~~bash
mkdir ~/workspace
cd ~/workspace
git clone https://gitlab.com/larswirzenius/vmdb2
cd vmdb2
sudo pip3 install .
~~~

Nachdem wir *vmdb2* installiert haben, können wir jetzt *image-specs* herunterladen und ein Debian 10 (Buster) Image für den **Raspberry Pi 4** ohne weitere Anpassungen generieren, um zu testen, ob alles funktioniert. Wenn ihr andere Images benötigt, könnt ihr den Befehl einfach anpassen. Ihr müsst immer *raspi*, dann die Raspberry Pi Generation (1, 2, 3, 4), dann die Debianversion (buster oder bullseye) und schließlich *.img* schreiben.

~~~bash
cd ~/workspace
git clone --recursive https://salsa.debian.org/raspi-team/image-specs.git
cd image-specs
# create Debian Buster image for Raspberry Pi 4
sudo make raspi_4_buster.img
~~~

Das erstelle Image könnt ihr dann auf eine SD-Karte kopieren und damit euren Raspberry Pi starten. Eine kleine Anleitung findet ihr am Ende dieses [Artikels](https://lookslikematrix.de/server/2021/02/24/debian-raspberry-pi-64bit.html).

Wenn ihr jetzt Anpassungen an eurem Image haben wollt, dann müsst ihr euch etwas mit [*vmdb2*](https://vmdb2-manual.liw.fi/) auseinandersetzen. Eine der einfachsten Anpassungen zeige ich euch hier kurz und zwar wie ihr weitere Pakete zu eurem Image installiert. Wenn ihr beispielsweise *git* standardmäßig in eurem Image haben möchtet, dann geht das wie folgt. Zunächst räumen wir das Verzeichnis auf, da beim **sudo make ...** ein paar *yaml*-Dateien erzeugt werden, die ich jetzt hier verwerfen möchte.

~~~bash
cd ~/workspace/image-specs
git clean -f
~~~

Anschließend öffnen wir die *raspi_master.yaml*-Datei und suchen den Block der mit **-apt: install** beginnt. Jetzt könnt ihr *git* wie folgt eintragen und mit *sudo make raspi_4_buster.img* ein eigenes Image erzeugen, dass standardmäßig *git* installiert hat (das Plus kennzeichnet lediglich die Zeile die hinzugefügt wurde und darf nicht mit die Datei kopiert werden).

~~~diff
  - apt: install
    packages:
+   - git
    - ssh
~~~

Wenn ihr euch weiter mit *vmdb2* beschäftigt, könnt ihr hier alles erreichen, was ihr von eurem eigenen Image erwartet. Ich hoffe die Anleitung hat euch geholfen. Wenn ihr Fragen oder Anregungen habt, dann schreibt gerne ein Kommentar und sonst freue ich mich über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).
