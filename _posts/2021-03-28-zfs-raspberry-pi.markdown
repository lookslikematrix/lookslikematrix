---
layout: post
title:  "ZFS – Wie erstelle und mounte ich eine ZFS Festplatte mit dem Raspberry Pi?"
date:   2021-03-28 16:30:00 +0200
categories: server
image: /assets/zfs.png
---

![zfs]({{ page.image }})

Da ich für meinen [home-server](https://gitlab.com/lookslikematrix/home-server) eine externe Festplatte an meinem Raspberry Pi brauche, um dort die Daten der einzelnen Services zu speichern, habe ich mich etwas über Dateisysteme informiert und bin für den Anwendungsfall bei [ZFS](https://de.wikipedia.org/wiki/ZFS_(Dateisystem)) gelandet. *ZFS* bietet eine integrierte RAID-Funktionalität, einen prüfsummenbasierte Schutz vor Datenübertragungsfehlern und bietet dabei eine relativ einfache Art, um diese Komplexität zu konfigurieren, sowie um Backups zu erstellen. Ich möchte hier kurz erläutern, welche Schritte dafür notwendig sind.

Als erstes benötigt ihr ein 64 Bit Betriebssystem auf eurem Raspberry Pi. Wie ihr das macht, habe ich [hier](https://lookslikematrix.de/server/2021/02/24/debian-raspberry-pi-64bit.html) erklärt, es gibt aber auch andere Möglichkeiten wie *Rasberry Pi OS (64bit)* oder *Ubuntu*. Ich verwende ein Debian 10 (Buster) und muss daher *backports* Pakete aktivieren, die mir ermöglichen eine neuere Version von *ZFS* zu nutzen, als standardmäßig bei Debian 10 dabei ist. Dazu öffne ich mit **nano /etc/apt/sources.list** die Paketkonfiguration und nehme folgende Änderung vor (mit minus ist die Zeile gekennzeichnet, die ihr entfernen müsst und mit plus die Zeile dir ihr hinzufügen müsst).

~~~diff
deb http://deb.debian.org/debian buster main contrib non-free
deb http://deb.debian.org/debian-security buster/updates main contrib non-free
- # deb http://deb.debian.org/debian buster-backports main contrib non-free
+ deb http://deb.debian.org/debian buster-backports main contrib non-free
~~~

Habt ihr das erledigt, könnt ihr *ZFS* aus den *backports* Paketen wie folgt installieren. Ihr müsst dazu einmalig einen Dialog mit *OK* akzeptieren und ein paar Minuten warten.

~~~bash
# change to root user
su
apt update
apt install linux-headers-`uname -r`
apt install -t buster-backports zfsutils-linux
# press OK when there is a dialog about licenses
# it take a few minutes ~10min
~~~

Anschließen überprüft ihr, ob alle *ZFS*-Services auf eurem System laufen. Bei mir hatte der *zfs-load-module.service* ein Problem, das ich mit einem Neustart des Services beheben konnte.

~~~bash
systemctl status zfs*
# was needed because this service had an issue
systemctl restart zfs-load-module.service
~~~

Jetzt läuft euer *ZFS*-System. Es fehlt nur noch eine Festplatte, die ihr damit verwalten könnt. Ich habe dazu alle Partitionen auf meiner Festplatte entfernt und anschließend an den Raspberry Pi angeschlossen. Damit ihr die Festplatte über *ZFS* verwalten könnt benötigt ihr die **ID** der Festplatte. Einfach das Verzeichnis */dev/disk/by-id* vor dem anschließen und nachdem anschließen der Festplatte anschauen. Die Änderung ist dann eure angeschlossene Festplatte – in meinem Fall *ata-SAMSUNG_HD103SI_S1VSJ90Z491353*.

~~~bash
ls /dev/disk/by-id > /tmp/before_mount
# insert storage
ls /dev/disk/by-id > /tmp/after_mount
diff /tmp/before_mount /tmp/after_mount
~~~

Jetzt könnt ihr ein Verzeichnis anlegen, indem die Festplatte gemountet werden kann. Damit es zu keinen Problemen kommt, geben wir in dem Verzeichnis allen alle Rechte. Wenn ihr plant die Festplatte über ein öffentliches Netzwerk freizugeben, dann solltet ihr hier noch etwas mehr Zeit investieren, um nicht eine böse Überraschungen zu bekommen. Ich habe die Festplatte nur für mich zu Hause und kann die Einstellungen daher erst mal so lassen.

~~~bash
mkdir -p /mnt/data
chmod 777 /mnt/data
~~~

Jetzt erstellen wir mit der Festplatten **ID** einen *ZFS*-Pool der *tank* heißt und mounten dann diesen *tank* in unser angelegtes Verzeichnis.

~~~bash
# Create pool
zpool create tank ata-SAMSUNG_HD103SI_S1VSJ90Z491353
# Mount tank
zfs create -o mountpoint=/mnt/data tank/data
~~~

Wenn alles geklappt hat sollte **zfs list** eine ähnliche Ausgabe habe.

~~~bash
NAME        USED  AVAIL     REFER  MOUNTPOINT
tank        300K   899G       24K  /tank
tank/data    34K   899G       34K  /mnt/data
~~~

Jetzt könnt ihr noch die Festplatte über *NFS* freigeben und somit von eurem PC dort Daten speichern oder herunterladen. Dazu müssen wir *NFS* noch installieren und unser *tank/data* über NFS freigeben.

~~~bash
# NFS
apt install nfs-kernel-server
zfs set xattr=sa dnodesize=auto tank/data
zfs set sharenfs="on" tank/data
zfs share tank/data
~~~

Ihr könnt anschließend auf eurem PC (In meinem Fall ein Debian 10) die NFS-Freigabe mounten und dort Daten ablegen oder herunter kopieren. Das funktioniert wie folgt, wenn die IP-Adresse eures Raspberry Pis 192.168.1.10 ist.

~~~bash
sudo apt install nfs-common
sudo mount -t nfs 192.168.1.10:/ /mnt
~~~

Wenn ihr die Sicherheit über die *NFS*-Freigabe noch erhöhen möchtet, könnt ihr auch die *NFS*-Konfiguration anpassen. Dies hier definiert beispielsweise, dass nur IP-Adressen im 192.168.178.1.0/24 Bereich lesenden und schreiben Zugriff haben, sowie der automatischen Annahme (fsid=0), dass wenn ein Client das Verzeichnis mountet, dass */mnt/data* das Wurzelverzeichnis ist.

~~~bash
zfs set sharenfs="fsid=0,rw=192.168.1.0/24,no_subtree_check" tank/data
~~~

Ich hoffe euer *ZFS*-System läuft jetzt. Hier unten findet ihr noch detailliertere Informationen, wie ihr beispielsweise *Snapshots* erstellen könnt oder ein RAID-System aufsetzt. Wenn ihr noch Fragen oder Anregungen habt, dann schreibt einfach ein Kommentar und sonst freue ich mich, wenn über einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix).

---
\[1\]: [https://wiki.debian.org/ZFS](https://wiki.debian.org/ZFS)

\[2\]: [https://openzfs.github.io/openzfs-docs/man/8/zfsprops.8.html](https://openzfs.github.io/openzfs-docs/man/8/zfsprops.8.html)

\[3\]: [https://www.raspberrypi.org/documentation/configuration/nfs.md](https://www.raspberrypi.org/documentation/configuration/nfs.md)

\[4\]: [https://linux.die.net/man/5/nfs](https://linux.die.net/man/5/nfs)

\[5\]: [https://de.wikipedia.org/wiki/ZFS_(Dateisystem)](https://de.wikipedia.org/wiki/ZFS_(Dateisystem))
