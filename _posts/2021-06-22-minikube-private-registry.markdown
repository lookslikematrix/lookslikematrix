---
layout: post
title:  "minikube – Wie installiere ich minikube und verwende Container aus einem privaten Repository?"
date:   2021-06-22 21:35:00 +0200
categories: server
image: /assets/minikube.png
---

![minikube]({{ page.image }})

Damit man eine Kubernetes Cluster lokal auf seinem Computer testen kann, bevor es dann in der Cloud betrieben wird, kann man [**minikube**](https://minikube.sigs.k8s.io/) verwenden. **minikube** stellt ein Kubernetes Cluster für fast jeden Rechner bereit. Ich erkläre hier kurz wie man **minikube** installiert, wie man sich authentifiziert, um Container aus einem privaten Repository wie *GitLab* zu beziehen und wie man dort dann eine kleine Applikation startet.

Zunächst müssen wir **minikube** installieren. Da ich ein Debian-System habe kann ich folgenden Befehl verwenden. Wenn ihr ein anderes System verwendet, könnt ihr unter [https://minikube.sigs.k8s.io/docs/start/](https://minikube.sigs.k8s.io/docs/start/) die Befehle für euer System ermitteln.

~~~bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube start
~~~

**minikube** sollte jetzt gestartet sein. Wenn ihr jetzt Container aus einem privaten Repository verwenden möchtet, müsst ihr euch in **minikube** authentifizieren. Die Authentifizierung an eurem Computer genügt nicht, da **minikube** nichts von der Authentifizierung des Computers weiß. In meinem Fall benötigte ich die Anmeldung bei *GitLab*. Dazu werden in einer Console folgenden Befehle ausgeführt. Anstatt eures Passworts müsst ihr euch bei *GitLab* einen *AccessToken* mit *api* generieren und einfügen ([https://gitlab.com/-/profile/personal_access_tokens](https://gitlab.com/-/profile/personal_access_tokens)).

~~~bash
minikube addons configure registry-creds
#Do you want to enable AWS Elastic Container Re#gistry? [y/n]: n

#Do you want to enable Google Container Registry? [y/n]: n

#Do you want to enable Docker Registry? [y/n]: y
#-- Enter docker registry server url: registry.gitlab.com
#-- Enter docker registry username: christian.decker
#-- Enter docker registry password:

#Do you want to enable Azure Container Registry? [y/n]: n
#✅  registry-creds was successfully configured
minikube addons enable registry-creds
~~~

Anschließend könnt ihr dann zum Testen ein einfaches Deployment-File *deployment.yaml* erstellen, dass den privaten Container benötigt.

~~~yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: lookslikematrix
  name: lookslikematrix-deployment
  labels:
    app: lookslikematrix
spec:
  selector:
    matchLabels:
      app: lookslikematrix
  template:
    metadata:
      labels:
        app: lookslikematrix
    spec:
      containers:
      - name: lookslikematrix
        image: registry.gitlab.com/lookslikematrix/secretcontainer:latest
~~~

Das Deployment könnt ihr dann mit folgenden Befehl erzeugen.

~~~bash
kubectl create -f deployment.yaml
~~~

Solltet ihr mehrere Kubernetes Cluster auf eurem Computer konfiguriert haben, könnt ihr dies mit *kubectl config get-contexts* herausfinden. Falls dort kein **\*** for **minikube** steht, müsst ihr noch den Kontext mit folgendem Befehl wechseln und das Deployment erneut erzeugen.

~~~bash
kubectl config use-context minikube
~~~

Ich hoffe diese kurze Anleitung hat euch geholfen. Sollten Fragen auftauchen könnt ihr diese gerne in die Kommentare schreiben und wenn ihr Anregungen habt natürlich auch. Ansonsten freue ich mich immer über [Kaffee](https://www.buymeacoffee.com/lookslikematrix).

---
\[1\]: [https://minikube.sigs.k8s.io/](https://minikube.sigs.k8s.io/)

\[2\]: [https://minikube.sigs.k8s.io/docs/start/](https://minikube.sigs.k8s.io/docs/start/)

\[3\]: [https://minikube.sigs.k8s.io/docs/handbook/registry/](https://minikube.sigs.k8s.io/docs/handbook/registry/)

\[4\]: [https://gitlab.com/-/profile/personal_access_tokens]([https://gitlab.com/-/profile/personal_access_tokens])
