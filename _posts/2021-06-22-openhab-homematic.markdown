---
layout: post
title:  "OpenHAB – Wie betreibe ich OpenHAB in einem Kubernetes Cluster?"
date:   2021-06-22 10:15:00 +0200
categories: server
image: /assets/openhab.png
---

![OpenHAB]({{ page.image }})

[**OpenHAB**](https://www.openhab.org/) ist ein Tool, dass hilft die Komponenten der Hausautomatisierung in einem Service zusammenzuführen. Für mein [home-server](https://gitlab.com/lookslikematrix/home-server)-Projekt war es notwendig, dass ich **OpenHAB** in einem Kubernetes Cluster starte und dort das [**Homematic**](https://www.eq-3.com/products/homematic.html)-Binding installiere, damit ich meine **Homematic** Geräte (hauptsächlich Heizungsthermostate) dort steuern und analysieren kann. Damit ihr das auch zu Hause machen könnt erkläre ich im Folgenden die notwendigen Schritte. Um eine aktuelle Anleitung mit *Helm* zu erhalten, könnt ihr einfach das Repository öffnen: [https://gitlab.com/lookslikematrix/home-server/-/tree/master/openhab](https://gitlab.com/lookslikematrix/home-server/-/tree/master/openhab).

Zunächst einmal benötigt ihr ein [eigenes Kubernetes Cluster](https://lookslikematrix.de/raspberry-pi/2020/09/27/k3s.html) und ein gewisses [Grundverständnis](https://lookslikematrix.de/raspberry-pi/2020/10/17/k3s-application.html) von Kubernetes. Jetzt kann es los gehen. Für **OpenHAB** benötigt ihr zwei *Persistant Volume Claims (PVCs)* - eins für die Daten und eins für die Konfiguration. Dazu legt ihr eine Datei *openhab-pvcs.yaml* mit folgendem Inhalt an.

~~~yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  namespace: openhab
  name: openhab-pvc-data
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: local-path
  resources:
    requests:
      storage: 10Gi
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  namespace: openhab
  name: openhab-pvc-conf
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: local-path
  resources:
    requests:
      storage: 1Gi
~~~

Weiterhin benötigen wir ein *Deployment* von **OpenHAB**. Dabei ist für die Interaktion mit **Homematic** aktuell notwendig ein *Milestone*-Release (3.1) von **OpenHAB** zu verwenden, da die aktuelle stabile Version (3.0) nicht einfach funktioniert. Also legen wir dazu eine Datei mit dem Namen *openhab-deployment.yaml* an.

~~~yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: openhab
  name: openhab-deployment
  labels:
    app: openhab
spec:
  selector:
    matchLabels:
      app: openhab
  template:
    metadata:
      labels:
        app: openhab
    spec:
      containers:
      - name: openhab
        image: openhab/openhab:3.1.0.M5-alpine
        volumeMounts:
        - name: openhab-data
          mountPath: /openhab/userdata
        - name: openhab-conf
          mountPath: /openhab/conf
      volumes:
      - name: openhab-data
        persistentVolumeClaim:
          claimName: openhab-pvc-data
      - name: openhab-conf
        persistentVolumeClaim:
          claimName: openhab-pvc-conf
~~~

Damit wir dann noch auf **OpenHAB** zugreifen können (Port 8080 für die grafische Oberfläche ) und auch die Kommunikation zur **Homematic**-Zentrale (Port 9125) richtig funktioniert müssen wir noch *Services* in einer Datei *openhab-services.yaml* anlegen. Dazu müsst ihr die IP-Adresse eures Clusters anstatt *192.168.178.10* eintragen.

~~~yaml
apiVersion: v1
kind: Service
metadata:
  namespace: openhab
  name: openhab-frontend-service
spec:
  type: ClusterIP
  selector:
    app: openhab
  ports:
    - protocol: TCP
      name: service
      port: 8080
  externalIPs:
  - 192.168.178.10
---
apiVersion: v1
kind: Service
metadata:
  namespace: openhab
  name: openhab-homematic-service
spec:
  type: ClusterIP
  selector:
    app: openhab
  ports:
    - protocol: UDP
      name: xml-rpc
      port: 9125
  externalIPs:
  - 192.168.178.10
~~~

Jetzt können wir die Konfigurations-Dateien auf euer Cluster spielen.

~~~bash
kubectl create namespace openhab
kubectl create -f openhab-pvcs.yaml
kubectl create -f openhab-deployment.yaml
kubectl create -f openhab-services.yaml
~~~

Es können jetzt ein paar Minuten vergehen bis alles eingerichtet ist (OpenHAB-Container herunterladen, *PVC*s vorbereiten und OpenHAB starten, etc.). Aber dann könnt ihr unter [http://192.168.178.10:8080](http://192.168.178.10:8080) **OpenHAB** öffnen und konfigurieren. Ich habe erstmal nur das *Addon* für **Homematic** installiert und anschließend eingerichtet. Wichtig dabei ist, dass ihr als *Gateway Address* die IP-Adresse eurer **Homematic**-Zentrale eingebt und als *Callback Network Address* die IP-Adresse eures Cluster (also *192.168.178.10* in unserem Beispiel). Anschließend wird ein automatischer Scan gestartet und ihr bekommt alle **Homematic**-Geräte in **OpenHAB** angezeigt, die ihr dann in **OpenHAB** einbinden könnt.

Als letztes habe ich noch im [Repository](https://github.com/openhab/openhab-addons/tree/main/bundles/org.openhab.binding.homematic) des **Homematic**-Addons herausgefunden, dass die Zentrale nicht automatisch Änderungen an Datenpunkten wie Temperatur verschickt und musste somit noch ein *Rule* in **OpenHAB** anlegen, die mir jede Minute (*0 \* \* \* \* ? \**) eine Script (*ECMASCript (ECMA - 262 Edition 5.1)*) ausführt, dass ein *REFRESH* Kommando an die einzelnen Geräte der Zentrale schickt.

~~~javascript
itemRegistry.getItems().forEach(function(item) {
  if (item.hasTag("Temperature") | item.hasTag("Humidity")) {
    events.sendCommand(item.getName(), "REFRESH");
  }
})
~~~

Ich hoffe ich konnte euch mit der Beschreibung helfen euer eigenes **OpenHAB** in einem Kubernetes Cluster zu betreiben. Wenn ihr noch Fragen oder Anregungen habt, dann könnt ihr mir gerne ein Kommentar oder E-Mail schreiben. Ansonsten könnt ihr mir auch gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) ausgeben.

---
\[1\]: [https://www.openhab.org/](https://www.openhab.org/)

\[2\]: [https://www.eq-3.com/products/homematic.html](https://www.eq-3.com/products/homematic.html)

\[3\]: [https://github.com/openhab/openhab-addons/tree/main/bundles/org.openhab.binding.homematic](https://github.com/openhab/openhab-addons/tree/main/bundles/org.openhab.binding.homematic)
