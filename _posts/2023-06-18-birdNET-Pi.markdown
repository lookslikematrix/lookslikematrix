---
layout: post
title:  "BirdNET-Pi 🐦 – Welche Vögel habe ich im Garten?"
date:   2023-06-18 10:05:00 +0200
categories: tools
image: /assets/bird-net-pi.png
---

![BirdNET-Pi]({{ page.image }})

Ich bin fasziniert, wenn Leute Vögel erkennen können anhand des Aussehens. Ich bin noch faszinierter, wenn die jemand anhand der Melodie eines Vogels erkennen kann, welcher Vogel das ist. Leider kann ich das nicht, aber ich bin auf das Projekt **[BirdNET-Pi](https://github.com/mcguirepr89/BirdNET-Pi)** aufmerksam geworden und das kann das für mich übernehmen **BirdNET-Pi** ist ein Echtzeit Vogel Klassifikationssystem für den Raspberry Pi.

Ich hatte zu Hause noch einen Raspberry Pi 4 übrig und ein billiges USB-Mikrophon und dachte ich gebe dem Projekt mal einen Versuch und bin hoch begeistert. Ich werde hier kurz erläutern, wie ihr es selbst einrichtet.

Zunächst benötigt ihr ein 64 Bit Raspberry Pi OS, welches ihr über den [*Raspberry Pi Imager*](https://www.raspberrypi.com/software/) auf eine SD-Karte installieren könnt, indem ihr dort *Raspberry Pi OS (other)* selektiert und anschließend *Raspberry Pi OS Lite (64-bit)* auswählt. Über das ⚙️ Einstellungssymbol könnt ihr dann auch noch Voreinstellung am System vornehmen. Ich hab hier beispielsweise direkt **ssh** aktiviert und ein **Wifi** konfiguriert. Nachdem ihr dann das Betriebssystem auf der SD-Karte habt und in euren Raspberry Pi eingesteckt habt, könnt ihr am einfachsten den Raspberry Pi einfach mit Maus und Tastatur an einen Bildschirm anschließe und anschließend **BirdNET-Pi** installieren.

Laut \[1\] könnt ihr dann einfach mit folgendem Befehl **BirdNET-Pi** installieren.

~~~bash
curl -s https://raw.githubusercontent.com/mcguirepr89/BirdNET-Pi/main/newinstaller.sh | bash
~~~

Bei mir ist während der Installation ein Fehler aufgetaucht wegen einer fehlenden Komponente (`python3-venv`). Ich musste dann die Installation wiederholen, aber ich vermute, dass das nur ein temporäres Problem war, da die Komponenten im Installationsskript auftaucht.

Die Wiederholung der Installation habe ich wie folgt gemacht.

~~~bash
# install missing component
sudo apt install python3-venv
# delete BirdNET-Pi
/usr/local/bin/uninstall.sh && cd ~ && rm -drf BirdNET-Pi
# reinstall BirdNET-Pi
curl -s https://raw.githubusercontent.com/mcguirepr89/BirdNET-Pi/main/newinstaller.sh | bash
~~~

Danach erreicht ihr unter der IP-Adresse es Raspberry Pi im gesamten Netzwerk (Smartphone, Laptop, etc.) das **BirdNET-Pi** einfach http://IP-Adresse/ im Browser eingeben und ihr solltet so ein ähnliches Bild bekommen.

![BirdNET-Pi Browser](/assets/bird-net-pi-browser.png)

Ihr solltet als Erstes dann über die *drei Striche* auf *Tools* gehen beziehungsweise auf größeren Bildschirmen direkt auf Tools. Als Benutzername nehmt ihr einfach *birdnet* und lasst das Passwortfeld leer. Damit könnt ihr dann unter *Settings* die *Location* einstellen, an der ihr euch befindet. Ich bin mir nicht sicher, aber ich vermute, dass darüber auch die Plausibilität der Ergebnisse geprüft wird, da ich am Anfang auch mal einen Flamingo 🦩 erkannt habe und jetzt eigentlich nur noch mir bekannte Vogelarten 🐦 erkannt werden.

Wenn man möchte, kann man dann die Daten mit dem [BirdWeather-Projekt](https://app.birdweather.com/) teilen und dort auch sehen, welche Vogelarten andere erkennen.

Ich hoffe wie immer, dass der Beitrag euch helfen kann. Wenn ihr Fragen oder Anregungen habt, dann immer gerne her damit ❤️. Ansonsten könnt ihr mir auch gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) ☕ ausgeben.

---
\[1\]: [https://github.com/mcguirepr89/BirdNET-Pi](https://github.com/mcguirepr89/BirdNET-Pi)

\[2\]: [https://github.com/kahst/BirdNET-Analyzer](https://github.com/kahst/BirdNET-Analyzer)

\[3\]: [https://www.raspberrypi.com/software/](https://www.raspberrypi.com/software/)

\[4\]: [https://app.birdweather.com/](https://app.birdweather.com/)
