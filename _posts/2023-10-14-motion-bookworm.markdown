---
layout: post
title:  "motion 📹 – Wie baue ich eine IP-Kamera (Raspberry Pi Zero 2)?"
date:   2023-10-14 12:55:00 +0200
categories: raspberry-pi
image: /assets/motion.png
---

![Motion]({{ page.image }})

Wir haben ein 🦔 Igelhaus gebaut und würden gerade im Herbst beobachten, ob ein Igel einzieht oder nicht. Da keiner von uns die ganze Zeit vor dem Igelhaus warten möchte, war die Idee eine IP-Kamera anzubringen und dann zu schauen, ob das Igelhaus bezogen wird.

Ich möchte euch im Folgenden erläutern, wie ihr mit [**Motion**](https://motion-project.github.io), einem `Raspberry Pi Zero 2` und einer Kamera eine IP-Kamera baut.

Zunächst einmal verwenden wir das aktuelle Raspberry Pi OS Bookworm in der 64 Bit Lite Variante. Dazu verwenden wir den [`Raspberry Pi Imager`](https://www.raspberrypi.com/software/) und selektieren unter *OS Wählen -> Raspberry Pi OS (other)* `Raspberry Pi OS Lite (64-bit)` aus. Am besten stellt ihr mit dem ⚙-Symbol noch mindestens WLAN und eine Art des Zugriffs ein, damit ihr komplett *Headless* auf den Raspberry Pi zugreifen könnt. Nachdem ihr die SD-Karte vorbereitet habt, schließt ihr nun die Kamera am Raspberry-Pi an, steckt die SD-Karte ein und versorgt den Raspberry Pi mit Strom.


Verbindet euch jetzt mit dem Raspberry Pi und sorgt erst einmal dafür, dass das System auf dem neusten Stand ist.

~~~bash
sudo apt update
sudo apt upgrade -y
~~~

Für die IP-Kamera installieren wir dann **Motion**. Im Vergleich zu anderen Anleitungen, die ich im Internet gefunden habe, benötigen wir allerdings seit Bullseye (Vorgänger von Bookworm) `libcamera` und müssen das zusätzlich installieren (siehe [\[3\]](https://github.com/motioneye-project/motioneye/issues/2425)). Also den folgenden Codeblock ausführen.

~~~bash
sudo apt install libcamera-tools libcamera-v4l2 motion -y
~~~

Wenn ihr jetzt alles richtig gemacht habt, dann könnt ihr **Motion** mit dem folgenden Befehl starten. Das `libcamerify` ermöglicht es `motion` mit `libcamera` zu starten, während ihr ohne einen Fehler bekommen würdet.

~~~bash
sudo libcamerify motion
~~~

Ihr solltet dann so eine ähnliche Ausgabe bekommen.

~~~log
[0:motion] [NTC] [ALL] conf_load: Processing thread 0 - config file /etc/motion/motion.conf
[0:motion] [NTC] [ALL] motion_startup: Logging to file (/var/log/motion/motion.log)
[0:00:59.734985081] [713] ERROR IPAModule ipa_module.cpp:172 Symbol ipaModuleInfo not found
[0:00:59.735449664] [713] ERROR IPAModule ipa_module.cpp:292 v4l2-compat.so: IPA module has no valid info
[0:00:59.735948258] [713]  INFO Camera camera_manager.cpp:284 libcamera v0.1.0+52-a858d20b
[0:00:59.883998987] [714]  WARN RPiSdn sdn.cpp:39 Using legacy SDN tuning - please consider moving SDN inside rpi.denoise
[0:00:59.890065341] [714]  INFO RPI vc4.cpp:387 Registered camera /base/soc/i2c0mux/i2c@1/ov5647@36 to Unicam device /dev/media3 and ISP device /dev/media0
[0:00:59.893818987] [713]  INFO Camera camera.cpp:1213 configuring streams: (0) 640x480-YUV420
[0:00:59.894656279] [714]  INFO RPI vc4.cpp:549 Sensor: /base/soc/i2c0mux/i2c@1/ov5647@36 - Selected sensor format: 640x480-SGBRG10_1X10 - Selected unicam format: 640x480-pGAA
[0:00:59.898540654] [713]  INFO Camera camera.cpp:1213 configuring streams: (0) 640x480-YUV420
[0:00:59.899285601] [714]  INFO RPI vc4.cpp:549 Sensor: /base/soc/i2c0mux/i2c@1/ov5647@36 - Selected sensor format: 640x480-SGBRG10_1X10 - Selected unicam format: 640x480-pGAA
~~~

Damit wir jetzt von eurem Smartphone oder Laptop auf die Kamera zugreifen können, müssen wir allerdings noch die Konfiguration anpassen. Dazu beenden wir mit *Strg + C* die Ausführung von **Motion** und öffnen die `motion.conf`-Datei.

~~~bash
sudo nano /etc/motion/motion.conf
~~~

Dort schalten wir den Zugriff auf das Webinterface und das Streaming im Netzwerk frei, indem wir folgende Anpassungen machen (Die Zeile mit dem Minus soll durch die mit dem Plus ersetzt werden). Danach mit *Strg + X* beenden und mit *Enter* speichern.

~~~diff
- webcontrol_localhost on
+ webcontrol_localhost off

- stream_localhost on
+ stream_localhost off
~~~

Wenn ihr jetzt nochmal **Motion** startet mit `sudo libcamerify motion`, dann solltet ihr über die IP-Adresse eures Raspberry Pis auf die Kamera zugreifen können. Die IP-Adresse findet ihr in der Regel mit `ip addr`. Und dann könnt ihr unter http://IP_ADDRESSE:8080 auf die Kamera zugreifen.

Als letztes wollen wir jetzt noch **Motion** automatisch beim Starten des Raspberry Pis starten. Dazu kommt `systemd` zum Einsatz, wofür wir den `motion`-Dienst anpassen müssen. Dazu öffnen wir die Datei.

~~~bash
sudo nano /lib/systemd/system/motion.service
~~~

Passen folgende Zeile an und speichern mit *Strg + X* und *Enter*.

~~~diff
- ExecStart=/usr/bin/motion
+ ExecStart=/usr/bin/libcamerify /usr/bin/motion
~~~

Da der Dienst unter dem `motion`-User gestartet wird, müssen wir zunächst die Logs entfernen, weil dort nach unseren ersten Starts nur `root` (mittels `sudo`) Schreibrechte hat. Anschließend können wir dann den Dienst aktivieren und starten.

~~~bash
sudo find /var/log/motion -delete
sudo mkdir /var/log/motion
sudo chown motion:motion /var/log/motion
sudo systemctl enable motion.service
sudo systemctl start motion.service
~~~

Ab jetzt solltet ihr immer beim Start eine IP-Kamera aus eurem Raspberry Pi erstellt haben. Wo ihr das Bild unter http://IP_ADDRESSE:8080 sehen könnt. Ihr könnt ja mal in den Kommentaren schreiben, wofür ihr die Kamera so im Einsatz habt. Ich hoffe, die Anleitung hat geholfen. Wenn ihr Fragen oder Anregungen habt, dann immer gerne her damit ❤️. Ansonsten könnt ihr mir auch gerne einen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) ☕ ausgeben.

---
\[1\]: [https://motion-project.github.io](https://motion-project.github.io)

\[2\]: [https://www.raspberrypi.com/software/](https://www.raspberrypi.com/software/)

\[3\]: [https://github.com/motioneye-project/motioneye/issues/2425](https://github.com/motioneye-project/motioneye/issues/2425)
