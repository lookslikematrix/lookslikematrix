---
layout: post
title:  "flatcar 🚆 – Wie installiere ich flatcar auf dem Raspberry Pi?"
date:   2024-05-24 14:25:00 +0200
categories: server
image: /assets/flatcar.png
---

Ich habe [hier](https://lookslikematrix.de/server/2024/05/24/flatcar.html) beschrieben, warum man [**flatcar**](https://www.flatcar.org) einsetzen könnte. Hier möchte ich beschreiben, wie ihr **flatcar** auf einem Raspberry Pi 4 installiert (siehe auch [\[3\]](https://www.flatcar.org/docs/latest/installing/bare-metal/raspberry-pi/)) und warum ihr bestimmte Schritte tun müsst. Aktuell (Stand Mai 2024) wird nur der [Raspberry Pi 4](https://www.flatcar.org/docs/latest/installing/bare-metal/raspberry-pi/) unterstützt.

**flatcar** benötigt ein System, das über [UEFI](https://en.wikipedia.org/wiki/UEFI) gebootet wird. Da der Raspberry Pi standardmäßig über den [EEPROM](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-boot-eeprom) bootet, müssen wir Vorbereitungen treffen, dass unsere Raspberry Pi über UEFI booten kann. Als Erstes aktualisieren wir den [Bootloader](https://en.wikipedia.org/wiki/Bootloader) des Raspberry Pis. Das ist der Teil der Software, der die Hardware initialisiert und damit alles vorbereitet, um das Betriebssystem zu starten. Die Aktualisierung ist notwendig, da dadurch auch der EEPROM aktualisiert wird, welcher ermöglicht, den Raspberry Pi über UEFI zu booten. Ich verlinke [hier](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#imager) die Dokumentation direkt von Raspberry Pi, da die Screenshots dort es am besten beschreiben. Da ich von einer SD-Karte booten werde, habe ich *SD Card Boot* ausgewählt, die SD-Karte dann angeschlossen, den Raspberry Pi mit Strom versorgt und eine grüne Anzeige auf dem angeschlossenen Monitor bekommen.

Der nächste Schritt ist es, die SD-Karte mit **flatcar** zu versehen. Hierfür stellt **flatcar** ein `flatcar-install`-Skript zur Verfügung, das ihr auf eurem Laptop ausführen könnt.

~~~bash
cd ~/Downloads
curl -LO https://raw.githubusercontent.com/flatcar/init/flatcar-master/bin/flatcar-install
chmod +x flatcar-install
~~~

Weiterhin benötigen wir eine `config.json`-Datei, die ihr im selben Verzeichnis wie das `flatcar-install`-Skript erstellt und in der ihr euren eigenen SSH-Public-Key (`cat ~/.ssh/id_rsa.pub`) eintragt.

~~~json
{
  "ignition": {
    "config": {},
    "security": {
      "tls": {}
    },
    "timeouts": {},
    "version": "2.3.0"
  },
  "networkd": {},
  "passwd": {
    "users": [
      {
        "name": "core",
        "sshAuthorizedKeys": [
          <Insert your SSH Keys here>
        ]
      }
    ]
  },
  "storage": {
    "files": [
      {
        "filesystem": "OEM",
        "path": "/grub.cfg",
        "append": true,
        "contents": {
          "source": "data:,set%20linux_console%3D%22console%3DttyAMA0%2C115200n8%20console%3Dtty1%22%0Aset%20linux_append%3D%22flatcar.autologin%20usbcore.autosuspend%3D-1%22%0A",
          "verification": {}
        },
        "mode": 420
      }
    ],
    "filesystems": [
      {
        "mount": {
          "device": "/dev/disk/by-label/OEM",
          "format": "btrfs"
        },
        "name": "OEM"
      }
    ]
  },
  "systemd": {}
}
~~~

Jetzt können wir **flatcar** auf die SD-Karte kopieren. Ich habe dafür einfach die vorherige SD-Karte genommen und über `gparted` formatiert. Die Adresse der SD-Karte findet ihr am einfachsten, wenn ihr den Befehl `lsblk` zweimal eingebt. Einmal ohne eingesteckte SD-Karte und einmal mit eingesteckter SD-Karte. Bitte hier vorsichtig sein, im Zweifel überschreibt ihr sonst im Folgenden einen anderen Datenträger auf eurem Laptop.

~~~bash
sudo ./flatcar-install -d /dev/sdX -C stable -B arm64-usr -o '' -i config.json
~~~

Abschließend noch UEFI auf die SD-Karte kopieren

~~~bash
efipartition=$(lsblk /dev/sdX -oLABEL,PATH | awk '$1 == "EFI-SYSTEM" {print $2}')
mkdir /tmp/efipartition
sudo mount ${efipartition} /tmp/efipartition
pushd /tmp/efipartition
version=$(curl --silent "https://api.github.com/repos/pftf/RPi4/releases/latest" | jq -r .tag_name)
sudo curl -LO https://github.com/pftf/RPi4/releases/download/${version}/RPi4_UEFI_Firmware_${version}.zip
sudo unzip RPi4_UEFI_Firmware_${version}.zip
sudo rm RPi4_UEFI_Firmware_${version}.zip
popd
sudo umount /tmp/efipartition
~~~

Wenn ihr jetzt die SD-Karte in den Raspberry Pi steckt, mit einem Netzwerkstecker verbindet und Strom anschließt, könnt ihr von eurem Laptop wie folgt darauf zugreifen. Ihr müsst geduldig sein, der Bootvorgang kann mehrere Minuten in Anspruch nehmen. Ich musste hier auch zweimal den Raspberry Pi zweimal starten und erst dann hat es funktioniert.

~~~bash
ssh core@IP_ADDRESS_OF_YOUR_PI
~~~

Damit haben wir es geschafft. **flatcar** ist auf dem Raspberry Pi installiert und ihr könnt nun beispielsweise `docker`-Kommandos ausführen.

~~~bash
docker run hello-world
~~~

Ich hoffe, diese Anleitung hat euch geholfen **flatcar** auf eurem Raspberry Pi einzurichten. Solltet ihr Anregungen oder Fragen haben, dann gerne einen Kommentar da lassen und ansonsten freue ich mich über einen virtuellen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) ☕.

---

\[1\]: [https://lookslikematrix.de/server/2024/05/24/flatcar.html](https://lookslikematrix.de/server/2024/05/24/flatcar.html)

\[2\]: [https://www.flatcar.org](https://www.flatcar.org)

\[3\]: [https://www.flatcar.org/docs/latest/installing/bare-metal/raspberry-pi/](https://www.flatcar.org/docs/latest/installing/bare-metal/raspberry-pi/)

\[4\]: [https://en.wikipedia.org/wiki/UEFI](https://en.wikipedia.org/wiki/UEFI)

\[5\]: [https://en.wikipedia.org/wiki/Bootloader](https://en.wikipedia.org/wiki/Bootloader)

\[6\]: [https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-boot-eeprom](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-boot-eeprom)
