---
layout: post
title:  "🕰️ RTC – Wie bekommt mein Raspberry offline die richtige Uhrzeit?"
date:   2024-10-27 12:49:00 +0100
categories: raspberry-pi
image: /assets/rtc.png
---

![rtc]({{ page.image }})

Ich betreibe einen [BirdNET-Pi](https://lookslikematrix.de/tools/2023/06/18/birdNET-Pi.html) zu Hause und möchte am Gymnasium hier im Ort einen offline [BirdNET-Pi](https://github.com/Nachtzuster/BirdNET-Pi) einrichten. Der Unterschied, zwischen den beiden ist, dass mein BirdNET-Pi online ist und der BirdNET-Pi am Gymnasium offline betrieben wird. In der Regel bezieht der Raspberry Pi seine Zeit über das Internet von einem [NTP](https://de.wikipedia.org/wiki/Network_Time_Protocol)-Server. Offline hat man logischerweise keinen Zugriff auf einen NTP-Server und kann die Zeit über eine Real-Time Clock (RTC) beziehen. Wenn ihr euren Computer startet, hat dieser in der Regel eine Knopfzelle (auch BIOS-Batterie genannt) verbaut, die unter anderem dafür sorgt, dass der Computer seine Zeit nicht vergisst und nach einem Neustart sofort die richtige Zeit hat. Wer sich mal gewundert hat, warum das *Raspberry Pi OS* nach einem Neustart in den ersten paar Sekunden ein falsches Datum hat, dann liegt es an der nicht vorhandenen Knopfzelle und einer fehlenden RTC. Für den offline BirdNET-Pi habe ich so eine RTC nachgerüstet und beschreibe euch hier, welche Schritte man machen muss. Ich habe hier vor allem diese Anleitung genommen: [https://pimylifeup.com/raspberry-pi-rtc/](https://pimylifeup.com/raspberry-pi-rtc/).

Ich habe mir dafür eine DS3231-RTC gekauft und diese mit den Pins 1, 3 und 5 am Raspberry Pi verbunden (siehe auch [\[4\]](https://pimylifeup.com/raspberry-pi-rtc/)). Ich musste eine Weile tüfteln, bis ich festgestellt habe, dass der Batterie-Kontakt an dem gekauften Board nicht richtig verlötet war. Daher gerne mal einen genauen Blick auf die Hardware treffen, damit ihr euch Zeit spart.

Als Nächstes habe ich den Raspberry Pi gestartet und den NTP-Server mit folgendem Befehl deaktiviert (siehe auch [\[5\]](https://pimylifeup.com/raspberry-pi-rtc/)).

```bash
sudo timedatectl set-ntp false
```

Anschließend musste ich I²C über `raspi-config` aktivieren. Den Eintrag findet man unter *Interface Options* -> *I2C* -> *Yes*. Das ist notwendig, damit die DS3231-RTC über I²C kommunizieren kann.

```bash
sudo raspi-config
```

Jetzt neu starten, die Tools installieren, die ihr für die Konfiguration benötigt, und testen, ob die RTC erkannt wird. Ihr solltet am Ende des folgenden Befehlsblocks den Eintrag `ID #68` sehen.

```bash
sudo reboot
sudo apt install python3-smbus i2c-tools
sudo i2cdetect -y 1
```

Anschließend müssen wir dem Raspberry Pi noch sagen, dass er ein bestimmtes Kernel-Module beim Booten laden muss. Das erfolgt in dem ihr am Ende der Datei `/boot/firmware/config.txt` eine Zeile hinzufügt. Die Datei mit `nano` öffnen und anschließend mit *Strg + X* und *Enter* schließen.

```bash
sudo nano /boot/firmware/config.txt
```

*Vorsicht: + meint nur, dass diese Zeile hinzugefügt wird. Nicht das + kopieren.*

```diff
+ dtoverlay=i2c-rtc,ds1307
```

Nach einem erneuten Neustart solltet ihr jetzt den Eintrag `ID #UU` ,anstatt `ID #68`, sehen.

Wenn ihr das geschafft habt, dann entfernen wir die `fake-hwclock`.

```bash
sudo apt -y remove fake-hwclock
sudo update-rc.d -f fake-hwclock remove
```

Und kommentiere die folgenden drei Zeilen in der `/lib/udev/hwclock-set`-Datei aus.

```bash
sudo nano /lib/udev/hwclock-set
```

*Vorsicht: - meint nur, dass die Zeile entfernt wird und + meint nur, dass diese Zeile hinzugefügt wird. Nicht das -/+ kopieren.*

```diff
-if [ -e /run/systemd/system ] ; then
-    exit 0
-fi
+#if [ -e /run/systemd/system ] ; then
+#    exit 0
+#fi
```

Jetzt sind wir so weit und können die Zeit der RTC setzen. Dazu einfach schauen, wie spät es gerade ist und im folgenden Befehl die Zeit tauschen.

```bash
sudo date --set='2024-10-27 12:49:00'
sudo hwclock -w
```

Jetzt könnt ihr noch mit `timedatectl` prüfen, ob die RTC-gesetzt wurde, und wenn ihr jetzt den Raspberry Pi herunterfahrt und neu startet, sollte die Uhrzeit direkt stimmen.

```bash
timedatectl
```

Ich hoffe, ihr habt der Anleitung gut folgen können. Solltet ihr noch Fragen oder Anregungen haben, dann kontaktiert mich gerne über Mail oder das Kommentarfeld. Und sonst freue ich mich natürlich immer über einen virtuellen [Kaffee](https://www.buymeacoffee.com/lookslikematrix) ☕.

---

\[1\]: [https://de.wikipedia.org/wiki/Network_Time_Protocol](https://de.wikipedia.org/wiki/Network_Time_Protocol)

\[2\]: [https://github.com/Nachtzuster/BirdNET-Pi](https://github.com/Nachtzuster/BirdNET-Pi)

\[3\]: [https://en.wikipedia.org/wiki/Real-time_clock](https://en.wikipedia.org/wiki/Real-time_clock)

\[4\]: [https://pimylifeup.com/raspberry-pi-rtc/](https://pimylifeup.com/raspberry-pi-rtc/)

\[5\]: [https://pimylifeup.com/raspberry-pi-time-sync/](https://pimylifeup.com/raspberry-pi-time-sync/)
