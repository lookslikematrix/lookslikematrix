---
layout: page
title: About
permalink: /about/
---

![Christian Decker](/assets/christian_decker.JPG)

Hi! Ich bin Christian und beschäftige mich seit 2013 mit Linux und allem was dazu gehört – Kommandozeile, Open-Source, Raspberry Pi und vielem mehr.

Wann immer meine Frau mich mit der Kommandozeile bei Linux arbeiten sieht, sagt sie immer "Das sieht aus wie bei Matrix" – es ist aber viel einfacher!

*lookslikematrix* ist ein OpenSource Blog. Ihr könnt unter [https://gitlab.com/lookslikematrix/lookslikematrix](https://gitlab.com/lookslikematrix/lookslikematrix) dazu beitragen.
