---
layout: page
title: Datenschutz
permalink: /dataprotection/
---

# Verwendung von Cookies

Wir verwenden Cookies, um den einwandfreien technischen Ablauf beim Besuch unserer Website zu gewährleisten und unser Internetangebot insgesamt nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, mit deren Hilfe wir spezifische Informationen auf Ihrem Endgerät abspeichern können. Sie dienen beispielsweise der temporären Speicherung Ihrer Log-In Daten, damit Sie beim Browser nicht permanent Ihre Registrierung erneuern müssen. Des Weiteren können Cookies auch dazu verwendet werden, die statistische Nutzung unserer Website zu messen.

Als User haben Sie die Möglichkeit, auf die Speicherung von Cookies Einfluss zu nehmen, indem Sie in den Einstellungen der Anwendung, mit der Sie unsere Website aufrufen und verwenden (z.B. Ihren Browser), das Speichern von Cookies einschränken oder verhindern. Wir weisen Sie darauf hin, dass die Nutzung unserer Website durch eine Sperrung von Cookies eingeschränkt werden kann.

# Statistische Auswertungen mit Matomo

Diese Website benutzt Matomo, eine Open-Source-Software [https://matomo.org/](https://matomo.org/) zur statistischen Auswertung der Besucherzugriffe. Zu diesem Zweck werden sog. „Cookies“ verwendet, das sind Textdateien, die auf Ihrem Computer gespeichert werden. Die durch die Cookies erzeugten Nutzungsinformationen werden an unseren Server übertragen und zu Nutzungsanalysezwecken gespeichert, was der Webseitenoptimierung unsererseits dient. Ihre IP-Adresse wird bei diesem Vorgang umgehend anonymisiert, so dass Sie als Nutzer für uns anonym bleiben. Der Server auf dem die statistischen Daten gespeichert werden ist von einem deutschen Anbieter und befindet sich auch physisch in Deutschland.

Wenn Sie mit der Speicherung und Auswertung dieser Daten aus Ihrem Besuch nicht einverstanden sind, dann können Sie der Speicherung und Nutzung nachfolgend per Mausklick jederzeit widersprechen. In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Matomo keinerlei Sitzungsdaten erhebt.

**Achtung**: Wenn Sie Ihre Cookies löschen, so hat dies zur Folge, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden muss.

<iframe style="border: 0; height: 200px; width: 100%;" src="https://matomo.lookslikematrix.de/index.php?module=CoreAdminHome&action=optOut&language=de"></iframe>
